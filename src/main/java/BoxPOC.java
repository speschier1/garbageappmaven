
import com.box.sdk.BoxAPIConnection;
import com.box.sdk.BoxConfig;
import com.box.sdk.BoxDeveloperEditionAPIConnection;
import com.box.sdk.BoxFile;
import com.box.sdk.BoxFolder;
import com.box.sdk.BoxItem;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author speschier
 */
public class BoxPOC {

    public BoxPOC() throws IOException {
        Logger.getLogger("org.jose4j.jwa").setLevel(Level.INFO);
        //BoxAPIConnection api = new BoxAPIConnection("Y6klfmemX91ftWR8otSzMtWi1J9ZYVbd");

        Reader reader = new FileReader("src/main/resources/box_piderpiper_config.json");
        BoxConfig boxConfig = BoxConfig.readFrom(reader);
        BoxDeveloperEditionAPIConnection api = BoxDeveloperEditionAPIConnection.getAppEnterpriseConnection(boxConfig);
        
        downloadFile(api,"632868743552");

    }

    private void listFilesAndFolders(BoxAPIConnection api){
        BoxFolder folder = BoxFolder.getRootFolder(api);
        
        for (BoxItem.Info itemInfo : folder) {
            if (itemInfo instanceof BoxFile.Info) {
                BoxFile.Info fileInfo = (BoxFile.Info) itemInfo;
                System.out.println(fileInfo.getName() + " - " + fileInfo.getID());
            } else if (itemInfo instanceof BoxFolder.Info) {
                BoxFolder.Info folderInfo = (BoxFolder.Info) itemInfo;
                System.out.println(folderInfo.getName() + " - " + folderInfo.getID());
            }
        }
    }
    
    private void downloadFile(BoxAPIConnection api, String fileId) throws IOException {
        BoxFile file = new BoxFile(api, fileId);
        BoxFile.Info info = file.getInfo();

        try (FileOutputStream stream = new FileOutputStream(info.getName())) {
            file.download(stream);
        }
    }

    public static void main(String[] args) {
        System.out.println(File.separator);
//        try {
//            new BoxPOC();
//        } catch (IOException ex) {
//            Logger.getLogger(BoxPOC.class.getName()).log(Level.SEVERE, null, ex);
//        }
    }

}
