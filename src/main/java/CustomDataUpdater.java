
import com.idexx.sap.me.sapmelib.common.DBEnum;
import com.idexx.sap.me.sapmelib.common.SiteEnum;
import com.idexx.sap.me.sapmelib.customData.CustomData;
import com.idexx.sap.me.sapmelib.customData.CustomDataDAO;
import com.idexx.sap.me.sapmelib.exceptions.SAPMELibException;
import garbage1.DatabaseConnectionFactory;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author speschier
 */
public class CustomDataUpdater {

    private DBEnum SAP_DATABASE = DBEnum.QA;
    private String OLD_USER = "LPRITCHARD";
    private String NEW_USER = "NKROOT";
    private String OBJ_ALIAS = "ROUTER";
    private String CUSTOM_FIELD_NAME = "NC_KU_COORDINATOR";

    public CustomDataUpdater() throws SQLException, SAPMELibException {

        List<String> activeRoutingHandles = getActiveRouting();
        for (String activeRoutingHandle : activeRoutingHandles) {
            updateCustomField(activeRoutingHandle);
        }

    }

    private void updateCustomField(String handle) throws SAPMELibException {
        CustomDataDAO lcd = new CustomDataDAO(SiteEnum.WESTBROOK, SAP_DATABASE);
        List<CustomData> customDataList = lcd.getCustomData(handle, OBJ_ALIAS);
        for (CustomData customData : customDataList) {
            if (customData.getName().equals(CUSTOM_FIELD_NAME)) {
                String name = (String) customData.getName();
                String value = (String) customData.getValue();
                if (!value.isEmpty() && value.toUpperCase().equals(OLD_USER)) {
                    lcd.saveCustomData(handle, OBJ_ALIAS, CUSTOM_FIELD_NAME, NEW_USER);
                    System.out.println(String.format("Updated Field:'%s' Handle:'%s' Old Value:'%s' New Value:'%s'", name, handle, value, NEW_USER));
                }
            }
        }
    }

    private List<String> getActiveRouting() throws SQLException {
        List<String> activeRoutingHandles = new ArrayList<>();
        Connection conn = null;
        Statement stmt = null;
        ResultSet rst = null;
        try {
            conn = DatabaseConnectionFactory.getHMQConnection();
            stmt = conn.createStatement();
            rst = stmt.executeQuery("select * from sapmewip.ROUTER where CURRENT_REVISION = 'true' and SITE = 'USPB'");

            while (rst.next()) {
                activeRoutingHandles.add(rst.getString("HANDLE"));
            }

        } finally {
            DatabaseConnectionFactory.closeOracleResultSet(rst);
            DatabaseConnectionFactory.closeOracleStatement(stmt);
            DatabaseConnectionFactory.closeOracleConnection(conn);
        }
        return activeRoutingHandles;
    }

    public static void main(String[] args) {
        try {
            new CustomDataUpdater();
        } catch (SQLException ex) {
            Logger.getLogger(CustomDataUpdater.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAPMELibException ex) {
            Logger.getLogger(CustomDataUpdater.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
