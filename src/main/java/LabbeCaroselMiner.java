
import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;
import garbage1.DatabaseConnectionFactory;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author speschier
 */
public class LabbeCaroselMiner {

    Connection conn = null;
    Statement stmt = null;
    ResultSet rst = null;
    private String INPUT_FILE = "C:\\Users\\speschier\\Desktop\\labbe_caro_data.csv";
    private String OUTPUT_FILE = "C:\\Users\\speschier\\Desktop\\labbe_car_combined_data.csv";
    private CSVWriter writer = null;
    private CSVReader reader = null;
    List<String> header = new ArrayList<>();

    public static final SimpleDateFormat DISPLAY_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    public static final SimpleDateFormat DISPLAY_DATE_FORMAT_ORACLE = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

    public LabbeCaroselMiner() throws IOException, SQLException, ParseException {

        File file = new File(OUTPUT_FILE);
        if (file.exists()) {
            file.delete();
        }

        writer = new CSVWriter(new FileWriter(OUTPUT_FILE), ',');

        try (CSVReader reader = new CSVReader(new FileReader(new File(INPUT_FILE)), ',', '\"', 0)) {
            conn = DatabaseConnectionFactory.getHMPConnection();
            List<String[]> dataSet = reader.readAll();
            String groupId = "-1";
            String appendData = "";
            for (String[] testData : dataSet) {
                
                String data = "";
                for (int i = 0; i < testData.length; i++) {
                    data += testData[i] + ",";
                }
                
                if (groupId.equals("-1")) {
                    data += "ASSEMBLED_DATE,COMPONENT_BO,DATA_ATTR";
                    writer.writeNext(data.split(","));
                    groupId = testData[testData.length - 1];
                    continue;
                }
                String serialNumber = testData[0];
                Date testDate = DISPLAY_DATE_FORMAT.parse(testData[1]);
                
                if (!groupId.equals(testData[testData.length - 1])) {
                    stmt = conn.createStatement();
                    String sql = String.format("select * from SAPMEWIP.SFC_ASSY A "
                            + "INNER JOIN SAPMEWIP.SFC_ASSY_DATA D ON D.SFC_ASSY_BO = A.HANDLE "
                            + "WHERE SFC_BOM_BO = 'SFCBOMBO:SFCBO:USPB,%s' "
                            + "AND ASSEMBLED_DATE < LOCALTOUTC (TO_TIMESTAMP('%s','MM/DD/YYYY HH24:MI:SS'), 'EST') "
                            + "AND COMPONENT_BO like '%%84216%%' ORDER BY ASSEMBLED_DATE DESC", serialNumber, DISPLAY_DATE_FORMAT_ORACLE.format(testDate));
                    rst = stmt.executeQuery(sql);
                    System.out.println(sql);
                    if (rst.next()) {
                        appendData = "";
                        appendData += rst.getString("ASSEMBLED_DATE") + ",";
                        appendData += rst.getString("COMPONENT_BO").split(",")[1] + ",";
                        appendData += rst.getString("DATA_ATTR");
                    }
                }
                groupId = testData[testData.length - 1];
                data += appendData;
                writer.writeNext(data.split(","));
            }
            
        } finally {
            DatabaseConnectionFactory.closeOracleResultSet(rst);
            DatabaseConnectionFactory.closeOracleStatement(stmt);
            DatabaseConnectionFactory.closeOracleConnection(conn);
        }
        writer.close();

    }

    public static void main(String[] args) {
        try {
            new LabbeCaroselMiner();
        } catch (Exception ex) {
            Logger.getLogger(LabbeCaroselMiner.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
