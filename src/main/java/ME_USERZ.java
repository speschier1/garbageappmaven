
import com.idexx.utils.LdapUtils;
import com.idexx.utils.model.ADSAuthenticator;
import com.idexx.utils.model.Employee;
import com.idexx.utils.model.active_directory.ADSearch;
import com.idexx.utils.model.active_directory.ADSearchEnum;
import garbage1.DatabaseConnectionFactory;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.directory.Attributes;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author speschier
 */
public class ME_USERZ {

    Connection conn = null;
    Statement stmt = null;
    ResultSet rst = null;

    public ME_USERZ() throws SQLException {
        List<String> users = new ArrayList<>();
        try {
            String sql = "select USER_ID from SAPMEWIP.USR";
            conn = DatabaseConnectionFactory.getHMPConnection();
            stmt = conn.createStatement();
            rst = stmt.executeQuery(sql);
            while (rst.next()) {
                users.add(rst.getString("USER_ID"));
            }
            DatabaseConnectionFactory.closeOracleStatement(stmt);
            DatabaseConnectionFactory.closeOracleResultSet(rst);
        } finally {
            DatabaseConnectionFactory.closeOracleResultSet(rst);
            DatabaseConnectionFactory.closeOracleStatement(stmt);
            DatabaseConnectionFactory.closeOracleConnection(conn);
        }
        //ADSAuthenticator ads = new ADSAuthenticator();
        ADSearch adsSearch = new ADSearch();
        String user = "SPESCHIER";
        //for (String user : users) {
            List<Attributes> attrb = adsSearch.searchType(ADSearchEnum.EMPLOYEE)
                    .filter("sAMAccountName", user)
                    .searchAttributes();
            if (!attrb.isEmpty()) {
                Optional<Employee> employee = LdapUtils.convertEmployee(attrb.get(0));
                    if (employee.get().getDistinguishedName().contains("DisabledAccounts")) {
                        System.out.println(user + " IS TERMINATED");
                    }

            } else {
                System.out.println(user + " IS NOT IN ACTIVE DIRECTORY");
            } 
                   
        //}
    }

    public static void main(String[] args) {
        try {
            new ME_USERZ();
        } catch (SQLException ex) {
            Logger.getLogger(ME_USERZ.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
