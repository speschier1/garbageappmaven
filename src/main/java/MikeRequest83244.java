
import au.com.bytecode.opencsv.CSVWriter;
import com.idexx.opsys.hematology.model.LaserCyteFCSFile;
import garbage1.DanLabbeDataCollectionMostRecentTest;
import garbage1.DatabaseConnectionFactory;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author speschier
 */
public class MikeRequest83244 {

    private CSVWriter writer = null;
    private String OUTPUT_FILE = "C:\\Users\\speschier\\Morin83244.csv";
    public static final SimpleDateFormat DISPLAY_DATE_FORMAT = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
    Connection conn = null;
    Statement stmt = null;
    ResultSet rst = null;

    List<String> cadillac_parameters = new ArrayList<>();
    List<String> fcs_parameters = new ArrayList<>();

    public MikeRequest83244() throws IOException {
        cadillac_parameters.add("IDXRunDateTime");
        cadillac_parameters.add("IDXSampleID");
        cadillac_parameters.add("IDXSpecies");
        cadillac_parameters.add("IDXUseBlueLEDForWhiteHGB");
        cadillac_parameters.add("HGBGain");
        cadillac_parameters.add("IDXHGBGain");
        cadillac_parameters.add("IDXRedHGBGain");
        cadillac_parameters.add("IDXWhiteCorrHGBGain");
        cadillac_parameters.add("IDXMCVGain");
        cadillac_parameters.add("IDXRBCGain");
        cadillac_parameters.add("IDXResult-HGB");
        cadillac_parameters.add("IDXResult-MCH");
        cadillac_parameters.add("IDXResult-MCHC");
        cadillac_parameters.add("IDXResult-MCV");
        cadillac_parameters.add("IDXResult-RBC");
        cadillac_parameters.add("IDXResult-RED HGB");
        cadillac_parameters.add("IDXResult-WHITE CORR HGB");
        cadillac_parameters.add("IDXResult-WHITE HGB");
        cadillac_parameters.add("IDXResult-WHITE HGB BLUE");
        cadillac_parameters.add("IDXResult-WHITE HGB GREEN");
        cadillac_parameters.add("IDXResult-WHITE SHEATH BLUE AVG");
        cadillac_parameters.add("IDXResult-WHITE SHEATH GREEN AVG");
        cadillac_parameters.add("IDXResult-WHITE SAMPLE BLUE AVG");
        cadillac_parameters.add("IDXResult-WHITE SAMPLE GREEN AVG");
        cadillac_parameters.add("IDXResult-WHITE AIR BLUE AVG");
        cadillac_parameters.add("IDXResult-WHITE AIR GREEN AVG");
        cadillac_parameters.add("IDXResult-RED SHEATH BLUE AVG");
        cadillac_parameters.add("IDXResult-RED SHEATH GREEN AVG");
        cadillac_parameters.add("IDXResult-RED SAMPLE BLUE AVG");
        cadillac_parameters.add("IDXResult-RED SAMPLE GREEN AVG");
        cadillac_parameters.add("IDXResult-RED AIR BLUE AVG");
        cadillac_parameters.add("IDXResult-RED AIR GREEN AVG");
        cadillac_parameters.add("IDXHGBGreenDarkADC");
        cadillac_parameters.add("IDXHGBBlueDarkADC");
        cadillac_parameters.add("IDXHGBGreenLightADC");
        cadillac_parameters.add("IDXHGBGreenLightDAC");
        cadillac_parameters.add("IDXHGBBlueLightADC");
        cadillac_parameters.add("IDXHGBBlueLightDAC");
        cadillac_parameters.add("HGBPupLightADCValue0");
        cadillac_parameters.add("HGBPupLightADCValue1");
        cadillac_parameters.add("HGBPupLightADCValue2");
        cadillac_parameters.add("HGBPupLightADCValue3");
        cadillac_parameters.add("HGBPupLightDACSetting0");
        cadillac_parameters.add("HGBPupLightDACSetting1");
        cadillac_parameters.add("HGBPupLightDACSetting2");
        cadillac_parameters.add("HGBPupLightDACSetting3");

        File file = new File(OUTPUT_FILE);
        if (file.exists()) {
            file.delete();
        }

        writer = new CSVWriter(new FileWriter(OUTPUT_FILE), ',');

        String header = "Serial Number,cadillac_parameters,hgb_target,mcv_target,rbc_target,";
        for (String parameter : cadillac_parameters) {
            header += parameter + ",";
        }
        header = header.substring(0, header.length() - 1);

        writer.writeNext(header.split(","));

        try {
            conn = DatabaseConnectionFactory.getOPSHEMAConnection();

            stmt = conn.createStatement();
            String sql = "select "
                    + "G.SERIAL, "
                    + "R.RUN_DATE, "
                    + "MAX(DECODE(PARAM_NAME,'FCS_FILE_LOCATION',PARAM_VALUE,null)) AS FCS_FILE_LOCATION, "
                    + "MAX(DECODE(TARGET_PARAMETER_NAME,'RBC',TARGET_PARAMETER_VALUE,null)) AS RBC_TARGET, "
                    + "MAX(DECODE(TARGET_PARAMETER_NAME,'MCV',TARGET_PARAMETER_VALUE,null)) AS MCV_TARGET, "
                    + "MAX(DECODE(TARGET_PARAMETER_NAME,'HGB',TARGET_PARAMETER_VALUE,null)) AS HGB_TARGET "
                    + "FROM CADILLAC_RUN_GROUP G "
                    + "INNER JOIN CADILLAC_RUN R ON R.CADILLAC_RUN_GROUP_ID = G.CADILLAC_RUN_GROUP_ID "
                    + "INNER JOIN CADILLAC_PARAM P ON R.CADILLAC_RUN_ID = P.CADILLAC_RUN_ID "
                    + "INNER JOIN CADILLAC_TARGET_PARAMETER TP ON TP.CADILLAC_TARGET_ID = R.CADILLAC_TARGET_ID "
                    + "WHERE (P.PARAM_NAME = 'FCS_FILE_LOCATION' OR TP.TARGET_PARAMETER_NAME = 'RBC' OR TP.TARGET_PARAMETER_NAME = 'MCV' OR TP.TARGET_PARAMETER_NAME = 'HGB') "
                    + "AND G.IS_PILOT = 0 AND R.IS_DELETED = 0 AND R.IS_FILTERED = 0 and R.RUN_DATE BETWEEN TO_DATE('2/1/2021','MM/DD/YYYY') AND TO_DATE('2/3/2021','MM/DD/YYYY') + 1 "
                    + "GROUP BY G.SERIAL,R.RUN_DATE ";
            System.out.println("running query " + new Date());
            rst = stmt.executeQuery(sql);
            System.out.println("processing results " + new Date());
            while (rst.next()) {
                String serial = rst.getString("SERIAL");
                String runDate = DISPLAY_DATE_FORMAT.format(rst.getDate("RUN_DATE"));
                String data = serial + ","
                        + runDate + ","
                        + rst.getString("HGB_TARGET") + ","
                        + rst.getString("MCV_TARGET") + ","
                        + rst.getString("RBC_TARGET") + ",";

                String path = rst.getString("FCS_FILE_LOCATION");
                path = path.replace("smb://", "\\\\").replace("/", "\\");
                File fcsFile = new File(path);
                LaserCyteFCSFile lcFile = new LaserCyteFCSFile(fcsFile);
                for (String cadillac_parameter : cadillac_parameters) {
                    data += lcFile.getParameter(cadillac_parameter).split(",")[0] + ",";
                }
                data = data.substring(0, data.length() - 1);
                System.out.println(new Date() + " " + path);
                writer.writeNext(data.split(","));
            }
        } catch (SQLException ex) {
            Logger.getLogger(DanLabbeDataCollectionMostRecentTest.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DatabaseConnectionFactory.closeOracleResultSet(rst);
            DatabaseConnectionFactory.closeOracleStatement(stmt);
            DatabaseConnectionFactory.closeOracleConnection(conn);
        }
        writer.close();

    }

    public static void main(String[] args) {
        try {
            new MikeRequest83244();
        } catch (IOException ex) {
            Logger.getLogger(MikeRequest83244.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
