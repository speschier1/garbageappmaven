//
//import au.com.bytecode.opencsv.CSVWriter;
//import com.idexx.opsys.hematology.model.LaserCyteFCSFile;
//import garbage1.DanLabbeDataCollectionMostRecentTest;
//import garbage1.DatabaseConnectionFactory;
//import java.io.File;
//import java.io.FileWriter;
//import java.io.IOException;
//import java.nio.file.Paths;
//import java.sql.Connection;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.sql.Statement;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//
///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
///**
// *
// * @author speschier
// */
//public class MorinMiner {
//
//    private CSVWriter writer = null;
//    private String OUTPUT_FILE = "C:\\Users\\speschier\\Desktop\\MorinSheath.csv";
//    public static final SimpleDateFormat DISPLAY_DATE_FORMAT = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
//    Connection conn = null;
//    Statement stmt = null;
//    ResultSet rst = null;
//
//    Connection connMHP = null;
//    Statement stmtMHP = null;
//    ResultSet rstMHP = null;
//
//    List<String> parameters = new ArrayList<>();
//
//    public MorinMiner() throws IOException {
//        parameters.add("Serial");
//        parameters.add("RUN_DATE");
//        parameters.add("RUN_TYPE");
//        parameters.add("RBCSheathVol");
//        parameters.add("RBCSheathVol2");
//        parameters.add("HGBPupLightDACSetting3");
//        parameters.add("RHGB_ROBUST_SLOPE");
//
//        File file = new File(OUTPUT_FILE);
//        if (file.exists()) {
//            file.delete();
//        }
//
//        writer = new CSVWriter(new FileWriter(OUTPUT_FILE), ',');
//
//        String header = "";
//        for (String parameter : parameters) {
//            header += parameter + ",";
//        }
//        header = header.substring(0, header.length() - 1);
//
//        writer.writeNext(header.split(","));
//
//        try {
//            conn = DatabaseConnectionFactory.getOPSHEMAConnection();
//            connMHP = DatabaseConnectionFactory.getSSPConnection();
//
//            stmt = conn.createStatement();
//            String sql = "select "
//                    + "G.SERIAL, "
//                    + "R.RUN_DATE, "
//                    + "MAX(DECODE(PARAM_NAME,'FCS_FILE_LOCATION',PARAM_VALUE,null)) AS FCS_FILE_LOCATION, "
//                    + "MAX(DECODE(TARGET_PARAMETER_NAME,'RBC',TARGET_PARAMETER_VALUE,null)) AS FCS_FILE_LOCATION, "
//                    + "MAX(DECODE(TARGET_PARAMETER_NAME,'MCV',TARGET_PARAMETER_VALUE,null)) AS FCS_FILE_LOCATION, "
//                    + "MAX(DECODE(TARGET_PARAMETER_NAME,'HGB',TARGET_PARAMETER_VALUE,null)) AS FCS_FILE_LOCATION "
//                    + "FROM CADILLAC_RUN_GROUP G "
//                    + "INNER JOIN CADILLAC_RUN R ON R.CADILLAC_RUN_GROUP_ID = G.CADILLAC_RUN_GROUP_ID "
//                    + "INNER JOIN CADILLAC_PARAM P ON R.CADILLAC_RUN_ID = P.CADILLAC_RUN_ID "
//                    + "INNER JOIN CADILLAC_TARGET_PARAMETER TP ON TP.CADILLAC_TARGET_ID = R.CADILLAC_TARGET_ID "
//                    + "WHERE (P.PARAM_NAME = 'FCS_FILE_LOCATION' OR TP.TARGET_PARAMETER_NAME = 'RBC' OR TP.TARGET_PARAMETER_NAME = 'MCV' OR TP.TARGET_PARAMETER_NAME = 'HGB') "
//                    + "AND G.IS_PILOT = 0 AND R.IS_DELETED = 0 AND R.IS_FILTERED = 0 and R.RUN_DATE BETWEEN TO_DATE('2/1/2021','MM/DD/YYYY') AND TO_DATE('2/8/2021','MM/DD/YYYY') + 1 "
//                    + "GROUP BY G.SERIAL,R.RUN_DATE ";
//
//            rst = stmt.executeQuery(sql);
//            while (rst.next()) {
//                String serial = rst.getString("SERIAL");
//                String runDate = DISPLAY_DATE_FORMAT.format(rst.getDate("RUN_DATE"));
//                String data = serial + ","
//                        + runDate + ","
//                        + rst.getString("RBCSHEATHVOL") + ","
//                        + rst.getString("RBCSHEATHVOL2") + ",";
//
//                String path = rst.getString("FCS_FILE_LOCATION");
//                path = path.replace("smb://", "\\\\").replace("/", "\\");
//                File fcsFile = new File(path);
//                LaserCyteFCSFile lcFile = new LaserCyteFCSFile(fcsFile);
//
//                data += lcFile.getParameter("HGBPupLightDACSetting3") + ",";
//                String dcGroup = "";
//                String name = "RED_HGB_ROBUST_SLOPE";
//                if (runType.equals("WB")) {
//                    dcGroup = "PRECISION_PF";
//                } else if (runType.contains("QC")) {
//                    dcGroup = "ACCURACY_PF";
//                } else {
//                    dcGroup = "CONFIRMATION_PF";
//                    name = "RHGB_SLOPE";
//                }
//
//                String inst_test_sql = String.format("select * from inst_test_data "
//                        + "where SFC = '%s' "
//                        + "and name = '%s' "
//                        + "and DC_GROUP = '%s' "
//                        + "and LOG_DATE > TO_DATE('%s','MM/DD/YYYY HH24:MI:SS') "
//                        + "order by log_date asc ", serial, name, dcGroup, runDate);
//
//                System.out.println(inst_test_sql);
//                stmtMHP = connMHP.createStatement();
//                rstMHP = stmtMHP.executeQuery(inst_test_sql);
//                if (rstMHP.next()) {
//                    data += rstMHP.getString("VALUE");
//                }
//
//                writer.writeNext(data.split(","));
//            }
//        } catch (SQLException ex) {
//            Logger.getLogger(DanLabbeDataCollectionMostRecentTest.class.getName()).log(Level.SEVERE, null, ex);
//        } finally {
//            DatabaseConnectionFactory.closeOracleResultSet(rst);
//            DatabaseConnectionFactory.closeOracleStatement(stmt);
//            DatabaseConnectionFactory.closeOracleConnection(conn);
//
//            DatabaseConnectionFactory.closeOracleResultSet(rstMHP);
//            DatabaseConnectionFactory.closeOracleStatement(stmtMHP);
//            DatabaseConnectionFactory.closeOracleConnection(connMHP);
//
//        }
//        writer.close();
//
//    }
//
//    public static void main(String[] args) {
//        try {
//            new MorinMiner();
//        } catch (IOException ex) {
//            Logger.getLogger(MorinMiner.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        String path = "smb://wmesmb.idexxi.com/production/Active/Lasercyte-Production/DXBP007344/2019-Oct-24/DXBP007344_191024_084613_White.FCS";
//        path = path.replace("smb://", "\\\\").replace("/", "\\");
//
//    }
//
//}
