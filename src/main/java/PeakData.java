
import au.com.bytecode.opencsv.CSVWriter;
import garbage1.DatabaseConnectionFactory;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PeakData {

    private CSVWriter writer = null;
    private String OUTPUT_FILE = "C:\\Users\\speschier\\Desktop\\Peaks.csv";
    public static final SimpleDateFormat DISPLAY_DATE_FORMAT = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

    public PeakData() throws IOException, SQLException {
        List<String> parameters = new ArrayList<>();
        parameters.add("SERIAL");
        parameters.add("SUBMIT_DATE");
        parameters.add("RUN_DATE");
        parameters.add("PARAM_NAME");
        parameters.add("PARAM_VALUE");

        File file = new File(OUTPUT_FILE);
        if (file.exists()) {
            file.delete();
        }
        
        writer = new CSVWriter(new FileWriter(OUTPUT_FILE), ',');
        
        String header = "";
        for (String parameter : parameters) {
            header += parameter + ",";
        }
        header = header.substring(0, header.length() - 1);

        writer.writeNext(header.split(","));

        
        List<String> serialList = getSerialList();

        List<String> runGroupList = getRunGroup(serialList);
        Connection conn = null;
        Statement stmt = null;
        ResultSet rst = null;

        try {
            conn = DatabaseConnectionFactory.getOPSHEMAConnection();
            for (String runGroup : runGroupList) {
                String sql = String.format("select * from CADILLAC_RUN_GROUP RG "
                        + "INNER JOIN CADILLAC_RUN R ON R.CADILLAC_RUN_GROUP_ID = RG.CADILLAC_RUN_GROUP_ID "
                        + "INNER JOIN CADILLAC_PARAM P ON P.CADILLAC_RUN_ID = R.CADILLAC_RUN_ID "
                        + "WHERE RG.CADILLAC_RUN_GROUP_ID = %s AND RG.IS_PILOT = 0 AND R.IS_DELETED = 0 AND R.IS_FILTERED = 0 AND P.PARAM_NAME "
                        + "IN ('IDXResult-RBC EXT PEAK MEAN','IDXResult-RED LATEX EXT PEAK MEAN','IDXResult-WHITE NEU CEN EXT PEAK','IDXResult-WHITE LATEX EXT PEAK MEAN','IDXResult-RBC TOF PEAK MEAN','IDXResult-RED LATEX TOF MEAN', "
                        + "'IDXResult-WHITE NEU TOF MEAN','IDXResult-WHITE LATEX TOF MEAN','IDXResult-RBC FSL PEAK MEAN','IDXResult-RED LATEX FSL PEAK MEAN','IDXResult-WHITE NEU CEN FSL PEAK','IDXResult-WHITE LATEX FSL PEAK MEAN', "
                        + "'IDXResult-RBC RAS PEAK MEAN','IDXResult-RED LATEX RAS PEAK MEAN','IDXResult-WHITE NEU CEN RAS PEAK','IDXResult-WHITE LATEX RAS PEAK MEAN','IDXResult-RBC FSH PEAK MEAN','IDXResult-RED LATEX FSH PEAK MEAN','IDXResult-WHITE LATEX FSH PEAK MEAN') ", runGroup);
                System.out.println(sql);
                stmt = conn.createStatement();
                rst = stmt.executeQuery(sql);

                while (rst.next()) {
                    String data = String.format("%s,%s,%s,%s,%s", rst.getString("SERIAL"), DISPLAY_DATE_FORMAT.format(rst.getDate("SUBMIT_DATE")), DISPLAY_DATE_FORMAT.format(rst.getDate("RUN_DATE")), rst.getString("PARAM_NAME"), rst.getString("PARAM_VALUE"));
                    writer.writeNext(data.split(","));
                }
                DatabaseConnectionFactory.closeOracleResultSet(rst);
                DatabaseConnectionFactory.closeOracleStatement(stmt);
            }
        } finally {
            DatabaseConnectionFactory.closeOracleResultSet(rst);
            DatabaseConnectionFactory.closeOracleStatement(stmt);
            DatabaseConnectionFactory.closeOracleConnection(conn);
        }

        writer.close();

    }

    private List<String> getRunGroup(List<String> serialList) throws SQLException {
        List<String> runGroupList = new ArrayList<>();
        Connection conn = null;
        Statement stmt = null;
        ResultSet rst = null;

        try {
            conn = DatabaseConnectionFactory.getOPSHEMAConnection();
            for (String string : serialList) {
                String serial = string.split(",")[0];
                String date_time = string.split(",")[1];

                String sql = String.format("select * from CADILLAC_RUN_GROUP WHERE SUBMIT_DATE < TO_DATE('%s','MM/DD/YYYY HH24:MI:SS') AND IS_PILOT = 0 AND SERIAL = '%s' ORDER BY SUBMIT_DATE DESC", date_time, serial);
                System.out.println(sql);
                stmt = conn.createStatement();
                rst = stmt.executeQuery(sql);
                if (rst.next()) {
                    runGroupList.add(rst.getString("CADILLAC_RUN_GROUP_ID"));
                }
                DatabaseConnectionFactory.closeOracleStatement(stmt);

            }
        } finally {
            DatabaseConnectionFactory.closeOracleResultSet(rst);
            DatabaseConnectionFactory.closeOracleStatement(stmt);
            DatabaseConnectionFactory.closeOracleConnection(conn);
        }

        return runGroupList;
    }

    private List<String> getSerialList() throws SQLException {
        List<String> serialList = new ArrayList<>();

        Connection conn = null;
        Statement stmt = null;
        ResultSet rst = null;

        String sql = "select * from sapmewip.ACTIVITY_log "
                + "where DATE_TIME BETWEEN TO_DATE('12/1/2019','MM/DD/YYYY') "
                + "AND TO_DATE('2/8/2020','MM/DD/YYYY') "
                + "and action_code IN ('DISPOSITION','COMPLETE') "
                + "and sfc like 'DXBP%' "
                + "and operation = 'LCDX_FINAL' "
                + "order by sfc,date_time ";

        try {
            conn = DatabaseConnectionFactory.getHMPConnection();
            stmt = conn.createStatement();
            rst = stmt.executeQuery(sql);
            while (rst.next()) {
                serialList.add(String.format("%s,%s", rst.getString("SFC"), DISPLAY_DATE_FORMAT.format(rst.getDate("DATE_TIME"))));
            }
            DatabaseConnectionFactory.closeOracleStatement(stmt);
            DatabaseConnectionFactory.closeOracleResultSet(rst);
        } finally {
            DatabaseConnectionFactory.closeOracleResultSet(rst);
            DatabaseConnectionFactory.closeOracleStatement(stmt);
            DatabaseConnectionFactory.closeOracleConnection(conn);
        }

        return serialList;
    }

    public static void main(String[] args) {
        try {
            new PeakData();
        } catch (IOException ex) {
            Logger.getLogger(PeakData.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(PeakData.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
