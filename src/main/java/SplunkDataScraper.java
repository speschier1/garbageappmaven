
import com.idexx.opsys.hematology.model.LaserCyteFCSFile;
import garbage1.CSVMonitorCrawler;
import garbage1.LaserCyteFCSCrawler;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.HttpClientBuilder;

public class SplunkDataScraper {

    final static private String SERVER_ENDPOINT = "http://172.23.68.137:8080/upload";
    final static private String APPLICATION_NAME = "CADILLAC";
    final static private String TEMP_FOLDER_LOCATION = "C:\\Users\\speschier\\splunk_fcs_files\\";
    public SplunkDataScraper() throws ParseException {

        String folderPath = "\\\\wmesmb.idexxi.com\\production\\Active\\Lasercyte-Production";
        //String folderPath = "\\\\wmesmb\\grp-vol1\\eSOP\\LaserCyte\\Lisol Histogram Files";
        String startDateString = "07/01/2021";
        Date startDate = new SimpleDateFormat("MM/dd/yyyy").parse(startDateString);
        String endDateString = "07/07/2021";
        Date endDate = new SimpleDateFormat("MM/dd/yyyy").parse(endDateString);

        File tempfolder = new File(TEMP_FOLDER_LOCATION);
        if(!tempfolder.exists()) {
            tempfolder.mkdirs();
        }
        
        try (Stream<Path> paths = Files.walk(Paths.get(folderPath))) {
            paths
                    .filter(Files::isRegularFile)
                    .filter(p -> new Date(p.toFile().lastModified()).after(startDate))
                    .filter(p -> new Date(p.toFile().lastModified()).before(endDate))
                    .filter(p -> p.toString().endsWith(".FCS"))
                    .forEach(this::processFile);
        } catch (IOException ex) {
            Logger.getLogger(CSVMonitorCrawler.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void processFile(Path file) {
        try {
            LaserCyteFCSFile lcFile = new LaserCyteFCSFile(file.toFile());

            Path tempFCSFile = Paths.get(TEMP_FOLDER_LOCATION + file.getFileName());
            lcFile.writeHeader2JSON(tempFCSFile.toFile());
            System.out.println(String.format("Sending File: %s", tempFCSFile.toFile().getAbsolutePath()));
            HttpResponse response = doPost(SERVER_ENDPOINT, tempFCSFile.toFile().getAbsolutePath(), APPLICATION_NAME);

            if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
                System.out.println(String.format("Server returned %s status code: %s",
                        response.getStatusLine().getStatusCode(),
                        response.getStatusLine().getReasonPhrase()));
                System.exit(1);
            }
            tempFCSFile.toFile().delete();
        } catch (IOException ex) {
            Logger.getLogger(LaserCyteFCSCrawler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public HttpResponse doPost(String endPoint, String fileName, String application) throws IOException {
        HttpClient client = HttpClientBuilder.create().build();
        HttpPost post = new HttpPost(endPoint);
        File file = new File(fileName);
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        builder.addTextBody("application", APPLICATION_NAME);
        builder.addBinaryBody("myFile", file, ContentType.DEFAULT_BINARY, file.getName());
        HttpEntity entity = builder.build();
        post.setEntity(entity);
        return client.execute(post);
    }

    public static void main(String[] args) {
        try {
            new SplunkDataScraper();
        } catch (ParseException ex) {
            Logger.getLogger(SplunkDataScraper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
