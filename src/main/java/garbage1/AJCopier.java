/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package garbage1;

import au.com.bytecode.opencsv.CSVReader;
import static garbage1.FCS_Coper.JAN_1_2020;
import static garbage1.FCS_Coper.JAN_1_2021;
import static garbage1.FCS_Coper.SOURCE_PATH;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

/**
 *
 * @author speschier
 */
public class AJCopier {

    private final String INPUT_FILE = "C:\\Users\\speschier\\OneDrive - IDEXX\\Desktop\\AJCopier.csv";
    //private final String SOURCE_DRIVE = "\\\\wmesmb.idexxi.com\\production\\Active\\Lasercyte-Production\\";
    private final String DEST_DRIVE = "L:\\ISOL\\EEVL Product Support\\ATheriault\\Reach\\Latex Mitigation\\.FCS Files\\ISOL .FCS";
    public static final List<String> serialList = new ArrayList<>();

//    String startDateString = "01/26/2021";
//    Date startDate = new SimpleDateFormat("MM/dd/yyyy").parse(startDateString);
//    String endDateString = "05/04/2021";
//    Date endDate = new SimpleDateFormat("MM/dd/yyyy").parse(endDateString);
    public AJCopier() throws Exception {

        try (CSVReader reader = new CSVReader(new FileReader(new File(INPUT_FILE)))) {
            List<String[]> dataSet = reader.readAll();
            for (String[] strings : dataSet) {
                if (!strings[0].startsWith("\\")) {
                    serialList.add("\\\\" + strings[0]);
                } else {
                    serialList.add(strings[0]);
                }
            }
        }
        long counter = 1;
        for (String string : serialList) {
            processFile(Paths.get(string));
            //System.out.println(String.format("%s of %s", counter++, serialList.size()));
        }

    }

    private void processFile(Path file) {
        Path destPathWhite = Paths.get(DEST_DRIVE + "\\" + file.getFileName());
        Path destPathRed = Paths.get(DEST_DRIVE + "\\" + file.getFileName().toString().replace("White", "Red"));
        try {
            Files.copy(file, destPathWhite, StandardCopyOption.REPLACE_EXISTING, StandardCopyOption.COPY_ATTRIBUTES);
            Files.copy(file, destPathRed, StandardCopyOption.REPLACE_EXISTING, StandardCopyOption.COPY_ATTRIBUTES);
        } catch (IOException ex) {
            Logger.getLogger(AJCopier.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

//    private void processFile(String serial, String date) throws Exception {
//        serial = serial.trim();
//        Date blah = CSV_FORMAT.parse(date);
//        date = FOLDER_FORMAT.format(blah);
//        System.out.println(date);
//        Path srcPath = Paths.get(SOURCE_DRIVE + "\\" + serial + "\\" + date);
//        Path destPath = Paths.get(DEST_DRIVE + "\\" + serial + "\\" + date);
//
//        Files.createDirectories(destPath.getParent());
//        Files.copy(srcPath, destPath, StandardCopyOption.REPLACE_EXISTING, StandardCopyOption.COPY_ATTRIBUTES);
//
//        System.out.println(serial + " - " + date);
//    }
    public static void main(String[] args) throws FileNotFoundException {
        try {
            new AJCopier();
        } catch (Exception ex) {
            Logger.getLogger(AJCopier.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
