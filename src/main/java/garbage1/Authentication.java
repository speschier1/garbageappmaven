/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package garbage1;

import java.net.MalformedURLException;
import java.net.URL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Authentication {

    private static final Logger LOG = LoggerFactory.getLogger(Authentication.class);
    private static final String WS_PROD_USERNAME = "wsuser";
    private static final String WS_PROD_PASSWORD = "idexx123";
    private static final String WS_QA_USERNAME = "wsuser";
    private static final String WS_QA_PASSWORD = "idexx123";
    private static final String WS_DEV_USERNAME = "wsuser";
    private static final String WS_DEV_PASSWORD = "idexx123";
    private static final String DEV_ADDRESS = "wmemhd01.idexxi.com";
    private static final String QA_ADDRESS = "wmemhq01.idexxi.com";
    private static final String PROD_ADDRESS = "wmemhp01.idexxi.com";

    private static final String DATABASE = "PROD";
    
    
    public Authentication() {
    }

    public static String getWSUsername() {
        String userName;
        switch (DATABASE) {
            case "DEV":
                userName = WS_DEV_USERNAME;
                break;
            case "QA":
                userName = WS_QA_USERNAME;
                break;
            case "PROD":
                userName = WS_PROD_USERNAME;
                break;
            default:
                userName = WS_DEV_USERNAME;
        }
        return userName;
    }

    public static String getWSPassword() {
        String userName;
        switch (DATABASE) {
            case "DEV":
                userName = WS_DEV_PASSWORD;
                break;
            case "QA":
                userName = WS_QA_PASSWORD;
                break;
            case "PROD":
                userName = WS_PROD_PASSWORD;
                break;
            default:
                userName = WS_DEV_PASSWORD;
        }
        return userName;
    }

    public static String getWSUrl(String path) {
        String urlString;
        URL url = null;
        try {
            url = new URL(path);
        } catch (MalformedURLException ex) {
            LOG.error(String.format("getWSUrl threw %s : %s", ex.getClass(), ex.getMessage()));
        }
        if (url == null) {
            return null;
        }
        switch (DATABASE) {
            case "DEV":
                urlString = getUrlString(url,DEV_ADDRESS);
                break;
            case "QA":
                urlString = getUrlString(url,QA_ADDRESS);;
                break;
            case "PROD":
                urlString = getUrlString(url,PROD_ADDRESS);;
                break;
            default:
                urlString = null;
        }
        return urlString;
    }

    private static String getUrlString(URL url, String address) {
        return String.format("%s://%s:%s%s", url.getProtocol(), address, url.getPort(), url.getPath());
    }
}
