/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package garbage1;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

/**
 *
 * @author speschier
 */
public class CSVMonitorCrawler {

    private static final String BASE_DIRECTORY = "\\\\wmerndmep\\idexx\\CTDX_CSV_FILES\\Archive";
    private static final DateFormat DATE_NETWORK_LOCATION_FORMAT = new SimpleDateFormat("yyyy-MMM-dd");
    private static final DateFormat CSV_FILE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    public CSVMonitorCrawler() {
        try (Stream<Path> paths = Files.walk(Paths.get(BASE_DIRECTORY), 1)) {
            paths
                    .filter(Files::isRegularFile)
                    .forEach(this::processFile);
        } catch (IOException ex) {
            Logger.getLogger(CSVMonitorCrawler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void processFile(Path file) {
        try {
            String fileName = file.getFileName().toString();
            System.out.println(file);
            int dateStart = fileName.indexOf("_") + 1;
            int dateEnd = fileName.indexOf("_", dateStart);
            String serialNumber = fileName.substring(0, dateStart - 1);
            Pattern p = Pattern.compile("CTDX\\d{6}");
            Matcher m = p.matcher(serialNumber);
            if (m.matches()) {
                String date = fileName.substring(dateStart, dateEnd);
                Path dir = Paths.get(BASE_DIRECTORY
                        + File.separator
                        + serialNumber
                        + File.separator
                        + DATE_NETWORK_LOCATION_FORMAT.format(CSV_FILE_FORMAT.parse(date))
                        + File.separator
                        + fileName);
                Files.createDirectories(dir.getParent());
                Files.move(file, dir, REPLACE_EXISTING);
            }
        } catch (IOException | ParseException ex) {
            Logger.getLogger(CSVMonitorCrawler.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("BROKEN: " + file);
        }

    }

    public static void main(String[] args) {
        new CSVMonitorCrawler();
    }
}
