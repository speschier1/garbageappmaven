package garbage1;

import au.com.bytecode.opencsv.CSVReader;
import com.idexx.opsys.hematology.common.HemaLibDBEnum;
import com.idexx.opsys.hematology.common.HemaLibException;
import com.idexx.opsys.hematology.lasercyte.bloodlab.BloodLabDAO;
import com.idexx.opsys.hematology.lasercyte.bloodlab.CBC5R;
import com.idexx.opsys.hematology.lasercyte.bloodlab.Target;
import com.idexx.opsys.hematology.lasercyte.cadillac.CadillacDAO;
import com.idexx.opsys.hematology.lasercyte.cadillac.CadillacPopulationData;
import com.idexx.opsys.hematology.lasercyte.cadillac.CadillacRun;
import com.idexx.opsys.hematology.lasercyte.cadillac.CadillacRunGroup;
import com.idexx.opsys.hematology.lasercyte.cadillac.PopulationDataList;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author speschier
 */
public class CadillacCopier {

    public CadillacCopier() throws IOException, HemaLibException {
        String CSV_PATH = "c:\\users\\speschier\\Desktop\\CadillacRuns.csv";
        CadillacDAO prodDAO = new CadillacDAO();
        CadillacDAO qaDAO = new CadillacDAO();

        BloodLabDAO prodBloodLabDAO = new BloodLabDAO();
        BloodLabDAO qaBloodLabDAO = new BloodLabDAO();
        Set<String> globalTargetListForPopulationData = new HashSet<>();

        try (CSVReader reader = new CSVReader(new FileReader(new File(CSV_PATH)), ',', '\"', 0)) {

            List<String[]> data = reader.readAll();

            for (String[] serialDate : data) {
                System.out.println("Processing " + serialDate[0]);
                List<CadillacRunGroup> prodRunGroup = prodDAO.getRunGroupBySerial(serialDate[0], HemaLibDBEnum.PRODUCTION);
                CadillacRunGroup correctRunGroup = null;
                for (CadillacRunGroup cadillacRunGroup : prodRunGroup) {
                    if (cadillacRunGroup.getStartDateFormated().equals(serialDate[1])) {
                        correctRunGroup = cadillacRunGroup;
                        break;
                    }
                }

                if (correctRunGroup != null) {
                    //get list of targets
                    Set<String> targetIds = new HashSet<>();
                    for (CadillacRun cadillacRun : correctRunGroup.getCadillacRuns()) {
                        targetIds.add(cadillacRun.getTarget().getTargetID() + "");
                    }

                    globalTargetListForPopulationData.addAll(targetIds);
                    //check if targets are there and save them if they are not.
                    for (String targetId : targetIds) {
                        Target qatarget = null;
                        try {
                            qatarget = qaBloodLabDAO.getTarget(targetId, HemaLibDBEnum.QA);
                        } catch (Exception ex) {

                        }

                        if (qatarget == null) {
                            Target prodTarget = prodBloodLabDAO.getTarget(targetId, HemaLibDBEnum.PRODUCTION);
                            qaBloodLabDAO.saveTarget(prodTarget, HemaLibDBEnum.QA);
                        }
                    }

                    //same thing for cbc5r
                    Set<String> cbc5rIds = new HashSet<>();
                    for (CadillacRun cadillacRun : correctRunGroup.getCadillacRuns()) {
                        cbc5rIds.add(cadillacRun.getCbc5r().getCbc5rId() + "");
                    }
                    for (String cbc5rId : cbc5rIds) {
                        CBC5R cbc5r = null;
                        try {
                            cbc5r = qaBloodLabDAO.getCbc5r(cbc5rId, HemaLibDBEnum.QA);
                        } catch (Exception ex) {

                        }
                        if (cbc5r == null) {
                            CBC5R prodCbc5r = prodBloodLabDAO.getCbc5r(cbc5rId, HemaLibDBEnum.PRODUCTION);
                            qaBloodLabDAO.saveCbc5r(prodCbc5r, HemaLibDBEnum.QA);
                        }
                    }
                    qaDAO.saveRunGroup(correctRunGroup, HemaLibDBEnum.QA);
                } else {
                    System.out.println("NOT FOUND " + serialDate[0] + " - " + serialDate[1]);
                }

            }
        }
        Connection conn = null;
        Statement stmt = null;
        ResultSet rst = null;

        try {
            PopulationDataList popDataList = new PopulationDataList();
            String queryString = String.format("SELECT * FROM CADILLAC_POP_DATA WHERE CADILLAC_TARGET_ID IN %s", createINStatement(globalTargetListForPopulationData));

            conn = DatabaseConnectionFactory.getOPSHEMAConnection();
            stmt = conn.createStatement();
            rst = stmt.executeQuery(queryString);

            while (rst.next()) {
                CadillacPopulationData popData = new CadillacPopulationData();
                popData.setCadillacPopulationDataId(rst.getLong("CADILLAC_POP_DATA_ID"));
                popData.setTargetID(rst.getLong("CADILLAC_TARGET_ID"));
                popData.setRunId(rst.getLong("CADILLAC_RUN_ID"));
                popData.setParamName(rst.getString("PARAM_NAME"));
                popData.setParamValue(rst.getDouble("PARAM_VALUE"));
                popData.setSlopeApplied(rst.getBoolean("SLOPE_APPLIED"));
                popDataList.getPopDataList().add(popData);
                
            }
            qaDAO.savePopData(popDataList, HemaLibDBEnum.QA);
        } catch (SQLException ex) {
            Logger.getLogger(CadillacCopier.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DatabaseConnectionFactory.closeOracleResultSet(rst);
            DatabaseConnectionFactory.closeOracleStatement(stmt);
            DatabaseConnectionFactory.closeOracleConnection(conn);
        }       

    }

    private String createINStatement(Set<String> IDList) {
        String inStatement = "(";
        for (String id : IDList) {
            inStatement += id + ",";
        }
        return inStatement.substring(0, inStatement.length() - 1) + ")";
    }

    public static void main(String[] args) {
        try {
            new CadillacCopier();
        } catch (IOException ex) {
            Logger.getLogger(CadillacCopier.class.getName()).log(Level.SEVERE, null, ex);
        } catch (HemaLibException ex) {
            Logger.getLogger(CadillacCopier.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
