/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package garbage1;

/**
 *
 * @author speschier
 */
public class CadillacOldRun {
    private String serial;
    private String runDate;
    private String runId;

    public CadillacOldRun(String serial, String runDate, String runId) {
        this.serial = serial;
        this.runDate = runDate;
        this.runId = runId;
    }

    /**
     * @return the serial
     */
    public String getSerial() {
        return serial;
    }

    /**
     * @param serial the serial to set
     */
    public void setSerial(String serial) {
        this.serial = serial;
    }

    /**
     * @return the runDate
     */
    public String getRunDate() {
        return runDate;
    }

    /**
     * @param runDate the runDate to set
     */
    public void setRunDate(String runDate) {
        this.runDate = runDate;
    }

    /**
     * @return the runId
     */
    public String getRunId() {
        return runId;
    }

    /**
     * @param runId the runId to set
     */
    public void setRunId(String runId) {
        this.runId = runId;
    }
    
    
}
