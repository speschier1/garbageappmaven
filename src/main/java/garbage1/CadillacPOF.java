//package garbage1;
//
//import Jama.Matrix;
//import java.math.BigDecimal;
//import java.math.RoundingMode;
//import org.apache.commons.math3.stat.StatUtils;
//
//public class CadillacPOF {
//
//    //DXBP014673
//    public static void main(String[] args) {
//        final double ITER_EPS = 0.0000001;
//        int iter = 0;
//        double previousSlope = 1, currentSlope = 0;
//        //double runDataRaw[] = new double[18];
//        //double runDataMultiDem[][] = new double[18][2];
//
//        double runDataRawPlus1[][] = {{1, 5.04}, {1, 5.15}, {1, 5.25}, {1, 5.28}, {1, 5.2}, {1, 5.49}, {1, 5.33}, {1, 5.01}, {1, 5.33}, {1, 5.02},
//        {1, 6.75}, {1, 6.18}, {1, 6.22}, {1, 2.32}, {1, 2.29}, {1, 2.2},
//        {1, 4.13}, {1, 4.05}};
////        double runDataRawPlus1[][] = {{5.04}, {5.15}, {5.25}, {5.28}, {5.2}, {5.49}, {5.33}, {5.01}, {5.33}, {5.02},
////        {6.75}, {6.18}, {6.22}, {2.32}, {2.29}, {2.2},
////        {4.13}, {4.05}};
//
//        double targetData[] = {7.62, 7.62, 7.62, 7.62, 7.62, 7.62, 7.62, 7.62, 7.62, 7.62,
//            9.18, 9.18, 9.18, 3.01, 3.01, 3.01,
//            5.69, 5.69};
//
//        for (int i = 0; i < 18; i++) {
////            runDataRaw[i] = runDataRawPlus1[i][1];
////            runDataMultiDem[i][0] = runDataRawPlus1[i][1];
////            runDataMultiDem[i][1] = targetData[i];
//        }
//
//        //SimpleRegression slope = new SimpleRegression(false);
//        //slope.addData(runDataMultiDem);
//        //System.out.println("slope = " + slope.getSlope());
//        //System.out.println("slope std err = " + slope.getSlopeStdErr() * 100);
//        Matrix X = new Matrix(runDataRawPlus1);
//        //print("X",X);
//        Matrix Y = new Matrix(targetData, targetData.length);
//        Matrix Xt = X.transpose();
//        Matrix W = Matrix.identity(18, 18);
//        int degreesOfFreedom = X.getColumnDimension();
//        double slope = calc(X, Xt, W, Y).get(0, 0);
//
//        //for (int iterations = 0; iterations < 7; iterations++) {
//        while (iter == 0 || Math.abs(previousSlope - currentSlope) > ITER_EPS) {
//            iter++;
//            if (iter > 50) {
//                break;
//            }
//            previousSlope = currentSlope;
//            Matrix B = calc(X, Xt, W, Y);
//            currentSlope = B.get(B.getRowDimension() - 1, 0);
//            Matrix residuals = Y.minus(X.times(B));
//
//            //print("B", B);
//            System.out.println("Degrees of Freedom: " + degreesOfFreedom);
//            System.out.println("Slope = " + currentSlope);
//            System.out.println("Intercept = " + B.get(0, 0));
//            System.out.println("Iterations = " + iter);
//
//            Matrix adjustedResiduals = new Matrix(residuals.getRowDimension(), residuals.getColumnDimension());
//            //print("residuals", residuals);
//            Matrix inverseXtX = (Xt.times(X)).inverse();
//            Matrix leverages = X.times(inverseXtX).times(Xt);
//            if (degreesOfFreedom <= 1) {
//                for (int j = 0; j < adjustedResiduals.getColumnDimension(); j++) {
//                    for (int i = 0; i < adjustedResiduals.getRowDimension(); i++) {
//                        adjustedResiduals.set(i, j, 100 * residuals.get(i, j));
//                    }
//                }
//            } else {
//
//                //print("leverages", leverages);
//                for (int j = 0; j < leverages.getColumnDimension(); j++) {
//                    for (int i = 0; i < leverages.getRowDimension(); i++) {
//                        if (i == j) {
//                            double resAdjusted = (Math.sqrt((1 - leverages.get(i, j))));
//                            resAdjusted = 1 / resAdjusted;
//                            resAdjusted = resAdjusted * residuals.get(i, 0);
//                            adjustedResiduals.set(i, 0, resAdjusted);
//                        }
//                    }
//                }
//            }
//            //print("adjustedResiduals", adjustedResiduals);
//            Matrix standardizedAdjustedResiduals = new Matrix(adjustedResiduals.getRowDimension(), adjustedResiduals.getColumnDimension());
//            double MAD;
//            double median = StatUtils.percentile(adjustedResiduals.getColumnPackedCopy(), 50);
//            double[] medianAdjustedResiduals = new double[18];
//            for (int i = 0; i < adjustedResiduals.getColumnPackedCopy().length; i++) {
//                medianAdjustedResiduals[i] = Math.abs(adjustedResiduals.getColumnPackedCopy()[i] - median);
//            }
//            MAD = StatUtils.percentile(medianAdjustedResiduals, 50) / .6745;
//
//            for (int j = 0; j < adjustedResiduals.getColumnDimension(); j++) {
//                for (int i = 0; i < adjustedResiduals.getRowDimension(); i++) {
//                    standardizedAdjustedResiduals.set(i, j, (1 / (4.685 * MAD)) * adjustedResiduals.get(i, j));
//                }
//            }
//            for (int j = 0; j < W.getColumnDimension(); j++) {
//                for (int i = 0; i < W.getRowDimension(); i++) {
//                    if (i == j) {
//                        double rStdSqr = Math.pow(standardizedAdjustedResiduals.get(i, 0), 2);
//                        double newWeight = 1 - rStdSqr;
//                        W.set(i, j, Math.pow((newWeight), 2));
//                    }
//                }
//            }
//
//            //this is better -corie
//            //Matrix paramVarMat = (Xt.times(W).times(X)).inverse();
//            Matrix paramVarMat = (Xt.times(X)).inverse();
//            double[] h = new double[18];
//
//            if (degreesOfFreedom > 1) {
//                for (int j = 0; j < leverages.getColumnDimension(); j++) {
//                    for (int i = 0; i < leverages.getRowDimension(); i++) {
//                        if (i == j) {
//                            h[j] = leverages.get(i, j);
//                        }
//                    }
//                }
//            } else {
//                for (int j = 0; j < leverages.getColumnDimension(); j++) {
//                    h[j] = 0.9999;
//                }
//            }
//
//            double robustSigma = CalculateRobustSigma(adjustedResiduals.getRowPackedCopy(), degreesOfFreedom, MAD, 4.685, h);
//            //double robustSigma = CalculateRobustSigma(standardizedAdjustedResiduals, W, degreesOfFreedom, MAD, 4.685, leverages);
//            
//            double MSEvalue = Math.pow(robustSigma, 2);
//
//            Matrix SE = paramVarMat.timesEquals(MSEvalue);
//            double stdErr = Math.sqrt(SE.get(0, 0));
//            System.out.println("Robust Slope StdErr = " + stdErr * 100);
//        }
//    }
//
//    private static Matrix calc(Matrix X, Matrix Xt, Matrix W, Matrix Y) {
//        return (Xt.times(W).times(X)).inverse().times(Xt).times(W).times(Y);
//    }
//
//    private static void print(String name, Matrix matrix) {
//        System.out.println("----------------" + name + "--------------------");
//        matrix.print(matrix.getColumnDimension(), 15);
//    }
//
//    //make sure the standardizedAdjustedResiduals.getRowDimension() = number of runs
//    public static double CalculateRobustSigma(Matrix standardizedAdjustedResiduals, Matrix Weights, int df, double MADsigma, double TUNING, Matrix leverages) {
//        //Matrix oneMinusLeverages = Matrix.identity(leverages.getRowDimension(), leverages.getColumnDimension()).minus(leverages);
//        double[] leveragesDiag = new double[18];
//
//        if (df > 1) {
//            for (int j = 0; j < leverages.getColumnDimension(); j++) {
//                for (int i = 0; i < leverages.getRowDimension(); i++) {
//                    if (i == j) {
//                        leveragesDiag[j] = 1 - leverages.get(i, j);
//                    }
//                }
//            }
//        } else {
//            for (int j = 0; j < leverages.getColumnDimension(); j++) {
//                leveragesDiag[j] = 1 - 0.9999;
//            }
//        }
//
//        Matrix oneMinusLeverages = new Matrix(leveragesDiag, leveragesDiag.length);
//        Matrix steve = Weights.times(oneMinusLeverages).times(Weights);
//        Matrix sum = (standardizedAdjustedResiduals.transpose()).times((steve)).times(standardizedAdjustedResiduals);
//        double madNumber = Math.pow(MADsigma * TUNING, 2) / (standardizedAdjustedResiduals.getRowDimension() - df);
//        Matrix something = sum.timesEquals(madNumber);  //TODO rename this
//
//        double sumOfStdAdjRes = 0.0;
//
//        for (int i = 0; i < sum.getRowDimension(); i++) {
//            sumOfStdAdjRes += sum.get(i, 0);
//        }
//
//        double weightTrace = Weights.trace();
//
//        return ((1 / weightTrace) + (df / standardizedAdjustedResiduals.getRowDimension()) * (sumOfStdAdjRes / Math.pow(weightTrace, 4))) * Math.sqrt(something.get(0, 0));
//
//    }
//
//    public static double CalculateRobustSigma(double[] data, int p, double s, double t, double[] h) {
////'   Compute robust sigma estimate of DuMouchel & O'Brien
////'   This function uses a formula from DuMouchel & O'Brien.  It is
////'   based on ideas in Huber, pp. 172-175 and 195-198.
//        final double DELTA = 0.01;
//        int n = data.length;
//
//        double st = s * t;
//        //double mean;
//        double[] u = new double[n];
//        double[] ul = new double[n];
//        double[] phi = new double[n];
//        double[] dphi = new double[n];
//        double[] phil = new double[n];
//        double[] bisquares = new double[n];
//        //double[] weights;
//        double mean1;
//        double mean2;
//        double k;
//        double sum = 0;
//
//        if (st == 0) {
//            return 0;
//        }
//
//        for (int i = 0; i < n; i++) {
//            u[i] = data[i] / st;
//        }
//
//        for (int i = 0; i < n; i++) {
//            if (Math.abs(u[i]) < 1.0) {
//                //bisquares[i] = (1 - u[i] ^ 2) ^ 2;   <-- from VB6 for reference
//                double temp = u[i] * u[i];
//                temp = 1 - temp;
//                bisquares[i] = temp * temp;
//            } else {
//                bisquares[i] = 0;
//            }
//        }
//
//        for (int i = 0; i < n; i++) {
//            phi[i] = u[i] * bisquares[i];
//        }
//        for (int i = 0; i < n; i++) {
//            ul[i] = u[i] + DELTA;
//        }
//
//        for (int i = 0; i < n; i++) {
//            if (Math.abs(ul[i]) < 1.0) {
//                double temp = ul[i] * ul[i];
//                temp = 1 - temp;
//                bisquares[i] = temp * temp;
//            } else {
//                bisquares[i] = 0;
//            }
//        }
//        for (int i = 0; i < n; i++) {
//            phil[i] = ul[i] * bisquares[i];
//        }
//        for (int i = 0; i < n; i++) {
//            dphi[i] = (phil[i] - phi[i]) / DELTA;
//        }
//
//        mean1 = calcMean(dphi);
//
//        for (int i = 0; i < n; i++) {
//            u[i] = dphi[i] - mean1;
//            u[i] *= u[i];
//        }
//
//        mean2 = calcMean(u);
//
////    'account for divide by zero error - JMH 10/8/04
//        if (mean1 != 0) {
//            k = 1 + (p / n) * (mean2 / (mean1 * mean1));
//        } else {
//            k = 0;
//        }
//
//        for (int i = 0; i < n; i++) {
//            sum += phi[i] * phi[i] * st * st * (1 - h[i]) / (n - p);
//        }
//
////    'acount for divide by zero error - JMH 10/8/04
//        if (mean1 != 0) {
//            return (k / Math.abs(mean1)) * Math.sqrt(sum);
//        } else {
//            return 100;
//        }
//
//    }
//
//    public final static int SCALE = 20;
//    public final static RoundingMode ROUND = RoundingMode.HALF_UP;
//
//    public static boolean isDoubleOK(double d) {
//        Double D = Double.valueOf(d);
//        return (!D.equals(Double.NEGATIVE_INFINITY)
//                && !D.equals(Double.POSITIVE_INFINITY)
//                && !D.equals(Double.NaN));
//    }
//
//    public static double calcMean(final double[] x) {
//        if (x.length == 0) {
//            return 0.0;
//        }
//        BigDecimal mean = BigDecimal.ZERO;
//        BigDecimal n = BigDecimal.valueOf(x.length)
//                .setScale(SCALE, ROUND);
//        if (n.longValue() > 0) {
//            for (double d : x) {
//                if (isDoubleOK(d)) {
//                    mean = mean.add(BigDecimal.valueOf(d)
//                            .setScale(SCALE, ROUND));
//                }
//            }
//            mean = mean.divide(n, ROUND);
//        }
//        return mean.doubleValue();
//    }
//
//}
