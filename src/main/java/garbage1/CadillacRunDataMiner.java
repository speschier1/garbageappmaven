/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package garbage1;

import au.com.bytecode.opencsv.CSVWriter;
import com.idexx.opsys.hematology.model.LaserCyteFCSFile;
import com.idexx.sap.me.sapmelib.SFC.SfcDAO;
import com.idexx.sap.me.sapmelib.SFC.ShopFloorControlBasic;
import com.idexx.sap.me.sapmelib.common.DBEnum;
import com.idexx.sap.me.sapmelib.common.SiteEnum;
import com.idexx.sap.me.sapmelib.exceptions.SAPMELibException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author speschier
 */
public class CadillacRunDataMiner {

    private CSVWriter writer = null;
    private String OUTPUT_FILE = "C:\\Users\\speschier\\Desktop\\JeremyLaserCyteCalData.csv";
    Connection conn = null;
    Statement stmt = null;
    ResultSet rst = null;
    List<String> parameters = new ArrayList<>();

    public CadillacRunDataMiner() throws IOException {

        parameters.add("IDXRunDateTime");
        parameters.add("InstrumentSerialNumber");
        parameters.add("IDXSampleID");
        parameters.add("IDXHGBBlueAirSheathRatio");
        parameters.add("IDXResult-RED SHEATH GREEN AVG");
        parameters.add("IDXResult-RED SHEATH BLUE AVG");
        parameters.add("IDXResult-RED SAMPLE BLUE AVG");
        parameters.add("IDXResult-RED SAMPLE GREEN AVG");
        parameters.add("IDXResult-RED AIR BLUE AVG");
        parameters.add("IDXResult-RED AIR GREEN AVG");
        parameters.add("IDXResult-WHITE SHEATH GREEN AVG");
        parameters.add("IDXResult-WHITE SHEATH BLUE AVG");
        parameters.add("IDXResult-WHITE SAMPLE BLUE AVG");
        parameters.add("IDXResult-WHITE SAMPLE GREEN AVG");
        parameters.add("IDXResult-WHITE AIR BLUE AVG");
        parameters.add("IDXResult-WHITE AIR GREEN AVG");

        Set<String> serialMap = new HashSet<>();
        serialMap.add("DXBP008765");
        serialMap.add("DXBP002533");
        serialMap.add("DXBP101212");
        serialMap.add("DXBP102634");
        serialMap.add("DXBP005762");
        serialMap.add("DXBP012724");
        serialMap.add("DXBP005935");
        serialMap.add("DXBP004918");
        serialMap.add("DXBP101369");
        serialMap.add("DXBP012423");
        serialMap.add("DXBP007942");
        serialMap.add("DXBP014374");
        serialMap.add("DXBP001553");
        serialMap.add("DXBP002815");
        serialMap.add("DXBP002930");
        serialMap.add("DXBP011878");
        serialMap.add("DXBP013225");
        serialMap.add("DXBP014979");
        serialMap.add("DXBP007786");
        serialMap.add("DXBP008556");
        ShopFloorControlBasic sfc = null;
        SfcDAO sfcDAO = new SfcDAO();
        if (true) {
            for (String serial : serialMap) {
                try {
                    sfc = sfcDAO.getStatus(serial, SiteEnum.WESTBROOK, DBEnum.PRODUCTION);
                } catch (SAPMELibException ex) {
                    Logger.getLogger(CadillacRunDataMiner.class.getName()).log(Level.SEVERE, null, ex);
                }
                if (!sfc.isDone()) {
                    System.out.println("############################" + sfc + " is not done! ############################################");
                    System.exit(1);
                }
            }
        }
        System.out.println(serialMap.size());

        File file = new File(OUTPUT_FILE);
        if (file.exists()) {
            file.delete();
        }

        writer = new CSVWriter(new FileWriter(OUTPUT_FILE), ',');

        String header = "";
        for (String parameter : parameters) {
            header += parameter + ",";
        }

        header += "IDXWhiteCorrHGBGain,HGB_TARGET";

        writer.writeNext(header.split(","));
        for (String serial : serialMap) {
            try {
                conn = DatabaseConnectionFactory.getHMPConnection();
                stmt = conn.createStatement();
                String sql = String.format("select * from CADILLAC_RUN R "
                        + "INNER JOIN CADILLAC_RUN_GROUP G ON G.CADILLAC_RUN_GROUP_ID = R.CADILLAC_RUN_GROUP_ID "
                        + "INNER JOIN CADILLAC_PARAM RP ON RP.CADILLAC_RUN_ID = R.CADILLAC_RUN_ID "
                        + "INNER JOIN CADILLAC_TARGET_PARAMETER P ON P.CADILLAC_TARGET_ID = R.CADILLAC_TARGET_ID "
                        + "INNER JOIN FLASHRECORD F ON F.SERIAL = G.SERIAL "
                        + "WHERE R.CADILLAC_RUN_GROUP_ID = (SELECT MAX(CADILLAC_RUN_GROUP_ID) FROM CADILLAC_RUN_GROUP WHERE SERIAL = '%s') AND "
                        + "F.PARAMETER_NAME = 'IDXWhiteCorrHGBGain' AND RP.PARAM_NAME = 'FCS_FILE_LOCATION' AND P.TARGET_PARAMETER_NAME = 'HGB' AND R.IS_DELETED = 0 AND R.IS_FILTERED = 0 ", serial);
                rst = stmt.executeQuery(sql);
                while (rst.next()) {
                    processFile(Paths.get(rst.getString("PARAM_VALUE")), rst.getString("PARAMETER_VALUE"), rst.getString("TARGET_PARAMETER_VALUE"));
                }
            } catch (SQLException ex) {
                Logger.getLogger(DanLabbeDataCollectionMostRecentTest.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                DatabaseConnectionFactory.closeOracleResultSet(rst);
                DatabaseConnectionFactory.closeOracleStatement(stmt);
                DatabaseConnectionFactory.closeOracleConnection(conn);
            }
        }
    }

    private void processFile(Path file, String flashValue, String targetValue) {
        System.out.println(file);
        try {
            LaserCyteFCSFile lcFile = new LaserCyteFCSFile(file.toFile());
            String row = "";
            for (String parameter : parameters) {
                if (parameter.contains("Flags")) {
                    row += lcFile.getParameter(parameter) != null ? lcFile.getParameter(parameter).replace(",", "|") : "";
                } else {
                    row += lcFile.getParameter(parameter) != null ? lcFile.getParameter(parameter).split(",")[0] : "";
                }
                row += ",";
            }
            row += flashValue + "," + targetValue;
            writer.writeNext(row.split(","));
        } catch (IOException ex) {
            Logger.getLogger(LaserCyteFCSCrawler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void main(String[] args) {
        try {
            new CadillacRunDataMiner();
        } catch (IOException ex) {
            Logger.getLogger(CadillacRunDataMiner.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
