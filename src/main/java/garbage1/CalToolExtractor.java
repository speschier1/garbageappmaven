package garbage1;

import au.com.bytecode.opencsv.CSVWriter;
import com.idexx.opsys.hematology.common.HemaLibDBEnum;
import com.idexx.opsys.hematology.common.HemaLibException;
import com.idexx.opsys.hematology.lasercyte.cadillac.CadillacDAO;
import com.idexx.opsys.hematology.lasercyte.cadillac.CadillacPopulationResult;
import com.idexx.opsys.hematology.lasercyte.cadillac.CadillacRun;
import com.idexx.opsys.hematology.lasercyte.cadillac.CadillacRunGroup;
import com.idexx.opsys.hematology.lasercyte.calc.CalcEngine;
import com.idexx.opsys.hematology.lasercyte.config.CadillacConfiguration;
import com.idexx.opsys.hematology.model.FCSParameters;
import com.idexx.opsys.hematology.utils.CadillacConstants;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author speschier
 */
public class CalToolExtractor {

    private HemaLibDBEnum DATABASE = HemaLibDBEnum.PRODUCTION;
    private static final List<String> parameters = new ArrayList<>();
    private String OUTPUT_FILE = "C:\\Users\\speschier\\Desktop\\CALTOOL_EXTRACT.csv";
    private CSVWriter writer = null;
    private String startDate = "1/2/2018";
    private String endDate = "2/8/2018";
    private CadillacDAO cadillacDAO = new CadillacDAO();

    public CalToolExtractor() {
        parameters.add("Session Date");
        parameters.add("Serial");
        parameters.add("Parameter");
        parameters.add("DateTime");
        parameters.add("SampleId");
        parameters.add("Raw");
        parameters.add("Target");
        parameters.add("Cal");
        parameters.add("Delta");
        parameters.add("Pop Mean");
        parameters.add("Flags");
        parameters.add("Cal Factors Flashed");
        parameters.add("Robust");
        parameters.add("Delta");
        List<Integer> idList = new ArrayList<>();
        idList.add(44630);
        idList.add(44629);
        idList.add(44628);
        idList.add(44626);
        idList.add(44623);
        idList.add(44622);
        File file = new File(OUTPUT_FILE);
        if (file.exists()) {
            file.delete();
        }

        try {
            writer = new CSVWriter(new FileWriter(OUTPUT_FILE), ',');
        } catch (IOException ex) {
            Logger.getLogger(CalToolExtractor.class.getName()).log(Level.SEVERE, null, ex);
            return;
        }

        String header = "";
        for (String parameter : parameters) {
            header += parameter + ",";
        }
        header = header.substring(0, header.lastIndexOf(",") - 1);
        writer.writeNext(header.split(","));

        Connection conn = null;
        Statement stmt = null;
        ResultSet rst = null;
        try {

//            conn = DatabaseConnectionFactory.getODPConnection();
//            stmt = conn.createStatement();
//            String sql = "SELECT CADILLAC_RUN_GROUP_ID FROM CADILLAC_RUN_GROUP WHERE CADILLAC_RUN_GROUP_ID IN ('38745','38683','38693','38970','38956','38981','38957','39033','39037')";
//            List<Long> idList = new ArrayList<>();
//            rst = stmt.executeQuery(sql);
//
//            while (rst.next()) {
//                idList.add(rst.getLong("CADILLAC_RUN_GROUP_ID"));
//            }
            CadillacConfiguration config = CadillacConstants.getConfiguration();

            for (long runGroupId : idList) {
                CadillacRunGroup runGroup = cadillacDAO.getRunsById(runGroupId, DATABASE);
                Collections.sort(runGroup.getCadillacRuns(), (CadillacRun ri2, CadillacRun ri1) -> {
                    return ri2.getRunDate().compareTo(ri1.getRunDate());
                });
                CalcEngine calcEngine = new CalcEngine(runGroup, true, config);

                calcEngine.analyze();

                for (FCSParameters parameter : FCSParameters.values()) {
                    //ONLY HGB!
                    // if (parameter.name().equals("RBC") || parameter.name().equals("MCV") || parameter.name().equals("HCT")) {
                    for (CadillacRun run : runGroup.getCadillacRuns()) {
                        //CHECK IF ITS DELETED OR FILTERED
                        if (!run.isDeleted() && !run.isFiltered()) {

                            double rawValue = Double.NaN;
                            if (run.getRawParameters().get(parameter) != null) {
                                rawValue = run.getRawParameters().get(parameter);
                            }
                            double targetValue = Double.NaN;
                            if (run.getTarget() != null) {
                                targetValue = run.getTarget().getParameterValue(parameter.getBloodTarget().getName());
                            }
                            double calibratedValue = Double.NaN;
                            if (run.getCalibratedParameters().get(parameter) != null) {
                                calibratedValue = run.getCalibratedParameters().get(parameter);
                            }
                            double delta = Double.NaN;
                            if (run.getDeltas().get(parameter) != null) {
                                delta = run.getDeltas().get(parameter);
                            }
//                                double cbc5r = Double.NaN;
//                                if (run.getCbc5r() != null) {
//                                    cbc5r = run.getCbc5r().getMean();
//                                }
                            String row = runGroup.getStartDateFormated() + "," + runGroup.getSerialNumber() + "," + parameter.getNameInResult() + ",";
                            row += run.getParameterByName(CadillacConstants.RUN_DATE_HEADER_NAME).getValue() + ",";
                            row += run.getParameterByName(CadillacConstants.SAMPLE_ID_HEADER_NAME).getValue() + ",";
                            row += CadillacConstants.round(rawValue, 3) + ",";
                            row += targetValue + ",";
                            row += CadillacConstants.round(calibratedValue, 3) + ",";
                            row += CadillacConstants.round(delta, 3) + ",";
                            List<CadillacPopulationResult> popDataResultList = updatePopData(runGroup);
                            if (!popDataResultList.isEmpty() && popDataResultList != null && run.getTarget() != null) {
                                for (CadillacPopulationResult popResult : popDataResultList) {
                                    if (popResult.getTargetId() == run.getTarget().getTargetID()
                                            && popResult.getParamName().equals(parameter.getNameInResult())) {
                                        row += popResult.getParamMeanValue() + ",";
                                        break;
                                    }
                                }
                            } else {
                                row += ",";
                            }
                            row += run.getFlags() != null ? run.getFlags() + "," : ",";

                            if (parameter.isDisplayCalFactor()) {
                                double rSlope = CadillacConstants.round(calcEngine.getRobustSlope(parameter.getNameInResult()), 2);
                                double flashedGain = runGroup.getFlashedGain(parameter);
                                row += flashedGain + ",";
                                row += rSlope + ",";
                                row += CadillacConstants.round(rSlope - flashedGain, 3);
                            }
                            writer.writeNext(row.split(","));
                        }
                    }
                    // }
                }
            }
            writer.close();
        } catch (HemaLibException | IOException ex) {
            Logger.getLogger(CalToolExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
//            DatabaseConnectionFactory.closeResultSet(rst);
//            DatabaseConnectionFactory.closeStatement(stmt);
//            DatabaseConnectionFactory.closeConnection(conn);
        }

    }

    private List<CadillacPopulationResult> updatePopData(CadillacRunGroup runGroup) {
        List<CadillacPopulationResult> popDataResultList = new ArrayList<>();
        Set<String> targetIDList = new HashSet<>();
        for (CadillacRun run : runGroup.getCadillacRuns()) {
            if (run.getTarget() != null) {
                targetIDList.add(run.getTarget().getTargetID() + "");
            }
        }
        if (!targetIDList.isEmpty()) {
            try {
                popDataResultList = cadillacDAO.getPopResults(targetIDList, DATABASE);
            } catch (HemaLibException ex) {
                Logger.getLogger(CalToolExtractor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return popDataResultList;
    }

    public static void main(String[] args) {
        new CalToolExtractor();
    }

}
