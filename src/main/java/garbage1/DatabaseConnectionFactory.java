package garbage1;

import java.sql.*;

/**
 * User: sdensmore <br />
 * Date: Nov 24, 2010 <br />
 * <br />
 *
 * @author sdensmore
 * @version 1.0
 */
public class DatabaseConnectionFactory {

    private static DatabaseConnectionFactory ocf = new DatabaseConnectionFactory();

    /**
     * Initialize the connection drivers this connection reference.
     */
    private DatabaseConnectionFactory() {
        try {
            DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param conn -
     * @param user -
     * @param pass -
     * @return a <code>java.sql.Connection</code> or null if unable to establish
     * the connection.
     */
    public static java.sql.Connection getOracleConnection(String conn, String user, String pass) {
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            return DriverManager.getConnection("jdbc:oracle:thin:@" + conn, user, pass);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static java.sql.Connection getHANAConnection(String conn, String user, String pass) {
        try {
            //Class.forName("com.sap.db.jdbcext.DataSourceSAP");
            return DriverManager.getConnection("jdbc:sap://" + conn, user, pass);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void closeOracleResultSet(ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException ignore) {
            }
        }
    }

    public static void closeOracleStatement(Statement stmt) {
        if (stmt != null) {
            try {
                stmt.close();
            } catch (SQLException ignore) {
            }
        }
    }

    public static void closeOracleConnection(Connection conn) {
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ignore) {
            }
        }
    }

    public static java.sql.Connection getHMPConnection() {
        return getHANAConnection("wmehana06:32315/HMP", "SASRPT", "MhdS4sRpt123");
    }

    public static java.sql.Connection getHMQConnection() {
        return getHANAConnection("wmehana05:32215/HMQ", "SASRPT", "MhqS4sRpt");
    }

    public static java.sql.Connection getHEPConnection() {
        return getHANAConnection("dbhep.idexxi.com:30015/HDB", "SASRPT", "VuNuvu6r#nAp");
    }

    public static java.sql.Connection getSSPConnection() {
        return getHANAConnection("wmehana06:33215/SSP", "INSTRTST", "Instrprd@123");
    }
    
    public static java.sql.Connection getOPSHEMAConnection() {
        return getOracleConnection("wmeracp3-scan.idexxi.com:1522:prhema", "ops_hema", "eUUOg029rpSmjq71w6vC");
    }
}
