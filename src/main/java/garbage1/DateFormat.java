package garbage1;

import java.text.ParseException;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author speschier
 */
public class DateFormat {

    public static void main(String[] args) throws ParseException {
//        SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss aa");
//        
//        Date firstParsedDate = dateFormat.parse("Mar 26, 2021 1:54:42 PM ");
//        Date secondParsedDate = dateFormat.parse("Mar 29, 2021 8:57:41 AM ");
//        long diff = secondParsedDate.getTime() - firstParsedDate.getTime();
//        System.out.println(firstParsedDate + " - " + secondParsedDate + " = " + diff);
        DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm:ss").withZone(ZoneId.systemDefault());
        final Calendar yesterday = Calendar.getInstance();
        yesterday.add(Calendar.DATE, -1);
        System.out.println(DATE_TIME_FORMATTER.format(yesterday.toInstant()));

    }
}
