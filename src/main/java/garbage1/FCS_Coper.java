/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package garbage1;

import com.idexx.opsys.hematology.model.LaserCyteFCSFile;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

/**
 *
 * @author speschier
 */
public class FCS_Coper {

    public static final String DEST_PATH = "\\\\dexter\\grp-vol1\\eSOP\\LaserCyte\\2020_FCS_FILES\\";
    public static final String SOURCE_PATH = "\\\\wmesmb.idexxi.com\\production\\Active\\Lasercyte-Production";
    public static final Date JAN_1_2020 = new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime();
    public static final Date JAN_1_2021 = new GregorianCalendar(2021, Calendar.JANUARY, 1).getTime();

    public FCS_Coper() {
        try (Stream<Path> paths = Files.walk(Paths.get(SOURCE_PATH))) {
            paths
                    .filter(Files::isRegularFile)
                    .filter(p -> new Date(p.toFile().lastModified()).after(JAN_1_2020))
                    .filter(p -> new Date(p.toFile().lastModified()).before(JAN_1_2021))
                    .filter(p -> p.toString().toUpperCase().endsWith(".FCS"))
                    .forEach(this::processFile);
        } catch (IOException ex) {
            Logger.getLogger(CSVMonitorCrawler.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void processFile(Path file) {
        Path destPath = Paths.get(DEST_PATH + file.toFile().getName());
        try {
            Files.copy(Paths.get(file.toUri()), destPath, StandardCopyOption.REPLACE_EXISTING, StandardCopyOption.COPY_ATTRIBUTES);
        } catch (IOException ex) {
            Logger.getLogger(FCS_Coper.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(file.toString());
    }

    public static void main(String[] args) {
        new FCS_Coper();
    }
}
