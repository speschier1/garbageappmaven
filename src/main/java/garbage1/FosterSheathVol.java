/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package garbage1;

import au.com.bytecode.opencsv.CSVWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author speschier
 */
public class FosterSheathVol {

    private Connection connSSP = null;
    private Statement stmtSSP = null;
    private ResultSet rstSSP = null;

    private Connection connOPSHEMA = null;
    private Statement stmtOPSHEMA = null;
    private ResultSet rstOPSHEMA = null;

    private CSVWriter writer = null;

    private static final String OUTPUT_FILE = "c:\\users\\speschier\\desktop\\FosterSheathVol.csv";
    public static final SimpleDateFormat DISPLAY_DATE_FORMAT = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

    public FosterSheathVol() throws IOException {

        File file = new File(OUTPUT_FILE);
        if (file.exists()) {
            file.delete();
        }

        writer = new CSVWriter(new FileWriter(OUTPUT_FILE), ',');

        List<String> parameters = new ArrayList<>();
        parameters.add("CADILLAC_RUN_GROUP_ID");
        parameters.add("SERIAL");
        parameters.add("SUBMIT_DATE");
        parameters.add("RBCSHEATHVOL");
        parameters.add("RBCSHEATHVOL2");
        parameters.add("RBCSHEATHVOLFELINE");
        parameters.add("RBCSHEATHVOLFERRET");

        String header = "";
        for (String parameter : parameters) {
            header += parameter + ",";
        }

        header += "RED_HGB_ROBUST_SLOPE";

        writer.writeNext(header.split(","));

        try {
            String opsHemaSQL = "select "
                    + "G.CADILLAC_RUN_GROUP_ID, "
                    + "G.SERIAL, "
                    + "G.SUBMIT_DATE, "
                    + "MAX(DECODE(PARAM_NAME,'RBCSheathVol',PARAM_VALUE,null)) AS RBCSheathVol, "
                    + "MAX(DECODE(PARAM_NAME,'RBCSheathVol2',PARAM_VALUE,null)) AS RBCSheathVol2, "
                    + "MAX(DECODE(PARAM_NAME,'RBCSheathVolFeline',PARAM_VALUE,null)) AS RBCSheathVolFeline, "
                    + "MAX(DECODE(PARAM_NAME,'RBCSheathVolFerret',PARAM_VALUE,null)) AS RBCSheathVolFerret "
                    + "FROM CADILLAC_RUN_GROUP G "
                    + "INNER JOIN CADILLAC_RUN R ON R.CADILLAC_RUN_GROUP_ID = G.CADILLAC_RUN_GROUP_ID "
                    + "INNER JOIN CADILLAC_PARAM P ON R.CADILLAC_RUN_ID = P.CADILLAC_RUN_ID "
                    + "WHERE RUN_DATE BETWEEN TO_DATE('10/23/2019','MM/DD/YYYY') AND TO_DATE('1/30/2020','MM/DD/YYYY')+1 "
                    + "AND "
                    + "( "
                    + "   P.PARAM_NAME = 'RBCSheathVol' OR P.PARAM_NAME = 'RBCSheathVol2' OR P.PARAM_NAME = 'RBCSheathVolFeline' OR P.PARAM_NAME = 'RBCSheathVolFerret' "
                    + ") "
                    + "GROUP BY SERIAL,SUBMIT_DATE,G.CADILLAC_RUN_GROUP_ID ";
            connSSP = DatabaseConnectionFactory.getSSPConnection();
            connOPSHEMA = DatabaseConnectionFactory.getOPSHEMAConnection();
            stmtOPSHEMA = connOPSHEMA.createStatement();
            rstOPSHEMA = stmtOPSHEMA.executeQuery(opsHemaSQL);
            while (rstOPSHEMA.next()) {
                String data = rstOPSHEMA.getString("CADILLAC_RUN_GROUP_ID") + ",";
                data += rstOPSHEMA.getString("SERIAL") + ",";
                data += DISPLAY_DATE_FORMAT.format(rstOPSHEMA.getDate("SUBMIT_DATE")) + ",";
                data += rstOPSHEMA.getString("RBCSHEATHVOL") + ",";
                data += rstOPSHEMA.getString("RBCSHEATHVOL2") + ",";
                data += rstOPSHEMA.getString("RBCSHEATHVOLFELINE") + ",";
                data += rstOPSHEMA.getString("RBCSHEATHVOLFERRET") + ",";

                stmtSSP = connSSP.createStatement();
                String sql = String.format("select * from inst_test_data "
                        + "where sfc = '%s' "
                        + "and log_date > to_date('%s','MM/DD/YYYY HH24:MI:SS') "
                        + "and DC_GROUP = 'PRECISION_PF' "
                        + "AND NAME = 'RED_HGB_ROBUST_SLOPE' "
                        + "order by log_date asc ", rstOPSHEMA.getString("SERIAL"), DISPLAY_DATE_FORMAT.format(rstOPSHEMA.getDate("SUBMIT_DATE")));
                System.out.println(sql);
                rstSSP = stmtSSP.executeQuery(sql);
                if (rstSSP.next()) {
                    data += rstSSP.getString("VALUE");
                }
                DatabaseConnectionFactory.closeOracleResultSet(rstSSP);
                DatabaseConnectionFactory.closeOracleStatement(stmtSSP);

                writer.writeNext(data.split(","));

            }
            writer.close();

        } catch (SQLException ex) {
            Logger.getLogger(FosterSheathVol.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DatabaseConnectionFactory.closeOracleResultSet(rstSSP);
            DatabaseConnectionFactory.closeOracleStatement(stmtSSP);
            DatabaseConnectionFactory.closeOracleConnection(connSSP);

            DatabaseConnectionFactory.closeOracleResultSet(rstOPSHEMA);
            DatabaseConnectionFactory.closeOracleStatement(stmtOPSHEMA);
            DatabaseConnectionFactory.closeOracleConnection(connOPSHEMA);
        }

    }

    public static void main(String[] args) {
        try {
            new FosterSheathVol();
        } catch (IOException ex) {
            Logger.getLogger(FosterSheathVol.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
