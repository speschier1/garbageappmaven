/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package garbage1;

import java.util.Arrays;
import org.apache.commons.math3.fitting.leastsquares.LeastSquaresBuilder;
import org.apache.commons.math3.fitting.leastsquares.LeastSquaresOptimizer;
import org.apache.commons.math3.fitting.leastsquares.LeastSquaresProblem;
import org.apache.commons.math3.fitting.leastsquares.LevenbergMarquardtOptimizer;
import org.apache.commons.math3.fitting.leastsquares.MultivariateJacobianFunction;
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;
import org.apache.commons.math3.stat.regression.SimpleRegression;
import org.apache.commons.math3.util.Pair;

/**
 *
 * @author speschier
 */
public class FourPlusFourEqualsMath {

    public static void slope() {
        SimpleRegression regression = new SimpleRegression(false);
//        double[][] points = {
//        {4.54,4.83,4.88,4.59,4.63,4.57,4.77,4.6,4.68,4.45,2.72,2.68,2.8,8.7,8.63,7.98,7,6.75},
//        {7.07,7.07,7.07,7.07,7.07,7.07,7.07,7.07,7.07,7.07,3.52,3.52,3.52,11.43,11.43,11.43,9.01,9.01 }};
//        regression.addData(points);

        regression.addData(4.54, 7.07);
        regression.addData(4.83, 7.07);
        regression.addData(4.88, 7.07);
        regression.addData(4.59, 7.07);
        regression.addData(4.63, 7.07);
        regression.addData(4.57, 7.07);
        regression.addData(4.77, 7.07);
        regression.addData(4.6, 7.07);
        regression.addData(4.68, 7.07);
        regression.addData(4.45, 7.07);
        regression.addData(2.72, 3.52);
        regression.addData(2.68, 3.52);
        regression.addData(2.8, 3.52);
        regression.addData(8.7, 11.43);
        regression.addData(8.63, 11.43);
        regression.addData(7.98, 11.43);
        regression.addData(7, 9.01);
        regression.addData(6.75, 9.01);

        System.out.println("getMeanSquareError = " + regression.getSlopeStdErr());
        System.out.println("getSlope = " + regression.getSlope());
        System.out.println("getRegressionSumSquares = " + regression.getRegressionSumSquares());
        System.out.println("getSumSquaredErrors = " + regression.getSumSquaredErrors());

        regression.regress();

        System.out.println("getMeanSquareError = " + regression.getSlopeStdErr());
        System.out.println("getSlope = " + regression.getSlope());

        final double radius = 70.0;
        final Vector2D[] observedPoints = new Vector2D[]{
            new Vector2D(4.54, 7.07),
            new Vector2D(4.83, 7.07),
            new Vector2D(4.88, 7.07),
            new Vector2D(4.59, 7.07),
            new Vector2D(4.63, 7.07),
            new Vector2D(4.57, 7.07),
            new Vector2D(4.77, 7.07),
            new Vector2D(4.6, 7.07),
            new Vector2D(4.68, 7.07),
            new Vector2D(4.45, 7.07),
            new Vector2D(2.72, 3.52),
            new Vector2D(2.68, 3.52),
            new Vector2D(2.8, 3.52),
            new Vector2D(8.7, 11.43),
            new Vector2D(8.63, 11.43),
            new Vector2D(7.98, 11.43),
            new Vector2D(7, 9.01),
            new Vector2D(6.75, 9.01),
            
        };

        // the model function components are the distances to current estimated center,
        // they should be as close as possible to the specified radius
        MultivariateJacobianFunction distancesToCurrentCenter = new MultivariateJacobianFunction() {
            @Override
            public Pair<RealVector, RealMatrix> value(final RealVector point) {

                Vector2D center = new Vector2D(point.getEntry(0), point.getEntry(1));

                RealVector value = new ArrayRealVector(observedPoints.length);
                RealMatrix jacobian = new Array2DRowRealMatrix(observedPoints.length, 2);

                for (int i = 0; i < observedPoints.length; ++i) {
                    Vector2D o = observedPoints[i];
                    double modelI = Vector2D.distance(o, center);
                    value.setEntry(i, modelI);
                    // derivative with respect to p0 = x center
                    jacobian.setEntry(i, 0, (center.getX() - o.getX()) / modelI);
                    // derivative with respect to p1 = y center
                    jacobian.setEntry(i, 1, (center.getX() - o.getX()) / modelI);
                }

                return new Pair<RealVector, RealMatrix>(value, jacobian);

            }
        };

        // the target is to have all points at the specified radius from the center
        double[] prescribedDistances = new double[observedPoints.length];
        Arrays.fill(prescribedDistances, radius);

        // least squares problem to solve : modeled radius should be close to target radius
        LeastSquaresProblem problem = new LeastSquaresBuilder().
                start(new double[]{100.0, 50.0}).
                model(distancesToCurrentCenter).
                target(prescribedDistances).
                lazyEvaluation(false).
                maxEvaluations(1000).
                maxIterations(1000).
                build();
        LeastSquaresOptimizer.Optimum optimum = new LevenbergMarquardtOptimizer().optimize(problem);
        Vector2D fittedCenter = new Vector2D(optimum.getPoint().getEntry(0), optimum.getPoint().getEntry(1));
        System.out.println("fitted center: " + fittedCenter.getX() + " " + fittedCenter.getY());
        System.out.println("RMS: " + optimum.getRMS());
        System.out.println("evaluations: " + optimum.getEvaluations());
        System.out.println("iterations: " + optimum.getIterations());
    }
    
    
    public static void main(String[] args) {
        
    }
}
