/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package garbage1;

import javafx.concurrent.Task;

/**
 *
 * @author speschier
 */
public class FunWithThreads {

    public FunWithThreads() {
        Thread t = new Thread(new adminThread());
        t.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {

            public void uncaughtException(Thread t, Throwable e) {
                System.out.println(t + " throws exception: " + e);
            }
        });
        // this will call run() function
        t.setDaemon(true);
        t.start();
    }

    public static void main(String[] args) {
        new FunWithThreads();
    }

    class adminThread implements Runnable {

        @Override
        public void run() {
            throw new RuntimeException();
        }
    }
}
