/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package garbage1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author speschier
 */
public class HANA {

    public HANA() {
       Connection connection = null;
      try {                  
         connection = DriverManager.getConnection(
            "jdbc:sap://wmehnq01.idexxi.com:30215/?autocommit=false","OPRRPT","Operations1");                  
      } catch (SQLException e) {
         System.err.println("Connection Failed. User/Passwd Error?");
         return;
      }
      if (connection != null) {
         try {
            System.out.println("Connection to HANA successful!");
            Statement stmt = connection.createStatement();
            ResultSet resultSet = stmt.executeQuery("SELECT * FROM \"_SYS_BIC\".\"IDEXX.OPR/MM_PO_GR\" WHERE EBELN = '4500737252'");
            resultSet.next();
            resultSet.setFetchSize(300);
            String hello = resultSet.getString("TXZ01");
            System.out.println(hello);
       } catch (SQLException e) {
          System.err.println("Query failed!");
       }
     }
        
    }
    
    
    public static void main(String[] args) {
        new HANA();
    }
    
}
