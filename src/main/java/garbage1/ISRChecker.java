/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package garbage1;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author speschier
 */
public class ISRChecker {

    public static final String FCS_RUN_DATE_PATTERN = "MM/dd/yyyy";
    public static final java.text.DateFormat DATE_FORMAT = new SimpleDateFormat(FCS_RUN_DATE_PATTERN);
    private static final String CSV_PATH = "c:\\users\\speschier\\desktop\\PLT_AND_RedCountRateFitSlope_data.csv";
    private static final String OUTPUT_FILE = "c:\\users\\speschier\\desktop\\PLT_AND_RedCountRateFitSlope_data_isr_data.csv";
    private CSVWriter writer = null;

    public ISRChecker() throws FileNotFoundException, IOException {
        File file = new File(OUTPUT_FILE);
        if (file.exists()) {
            file.delete();
        }

        writer = new CSVWriter(new FileWriter(OUTPUT_FILE), ',');
List<String> parameters = new ArrayList<>();
        parameters.add("sap");
        parameters.add("IDXAnalyzerSerialNumber");
        parameters.add("ivls");
        parameters.add("IDXRunDateTime");
        parameters.add("IDXResult-PLT");
        parameters.add("IDXResult-RED COUNT RATE FIT SLOPE");
        parameters.add("IDXSpecies");
        
        String header = "";
        for (String parameter : parameters) {
            header += parameter + ",";
        }

        header += "ISR_DATE";

        writer.writeNext(header.split(","));
        
        
        Connection conn = null;
        Statement stmt = null;
        ResultSet rst = null;
        try {
            conn = DatabaseConnectionFactory.getHEPConnection();
            try (CSVReader reader = new CSVReader(new FileReader(new File(CSV_PATH)),',','\"',1)) {
                List<String[]> data = reader.readAll();

                for (String[] strings : data) {
                    stmt = conn.createStatement();
                    String sql = String.format("select * from "
                            + "( "
                            + "   select "
                            + "   TO_DATE(ERDAT,'YYYYMMDD') \"ISR_DATE\" "
                            + "   from \"_SYS_BIC\".\"IDEXX.OPR/QMEL\" "
                            + "   where SERIALNR = '%s' "
                            + ") "
                            + "WHERE ISR_DATE >= TO_DATE('20191004','YYYYMMDD') "
                            + "order by ISR_DATE desc ", strings[1]);

                    rst = stmt.executeQuery(sql);
                    String date_string = "";
                    if (rst.next()) {
                        date_string = DATE_FORMAT.format(rst.getDate("ISR_DATE"));
                    }

                    DatabaseConnectionFactory.closeOracleResultSet(rst);
                    DatabaseConnectionFactory.closeOracleStatement(stmt);
                    header = "";
                    for (String parameter : strings) {
                        header += parameter + ",";
                    }

                    header += date_string;

                    writer.writeNext(header.split(","));

                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(DanLabbeDataCollectionMostRecentTest.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            DatabaseConnectionFactory.closeOracleResultSet(rst);
            DatabaseConnectionFactory.closeOracleStatement(stmt);
            DatabaseConnectionFactory.closeOracleConnection(conn);
            writer.close();
        }

    }

    public static void main(String[] args) {
        try {
            new ISRChecker();
        } catch (IOException ex) {
            Logger.getLogger(ISRChecker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
