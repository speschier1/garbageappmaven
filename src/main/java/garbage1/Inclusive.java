/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package garbage1;

/**
 *
 * @author speschier
 */
public class Inclusive {
    
    public static boolean specCheck(Double value, Double upperSpec, Double lowerSpec, boolean isInclusive) {
        boolean isPass;
        if (isInclusive) {
            if (lowerSpec != null && upperSpec != null) {
                isPass = value >= lowerSpec && value <= upperSpec;
            } else if (lowerSpec != null) {
                isPass = value >= lowerSpec;
            } else if (upperSpec != null) {
                return value <= upperSpec;
            } else {
                isPass = true;
            }
        } else if (lowerSpec != null && upperSpec != null) {
            isPass = value > lowerSpec && value < upperSpec;
        } else if (lowerSpec != null) {
            isPass = value > lowerSpec;
        } else if (upperSpec != null) {
            isPass = value < upperSpec;
        } else {
            isPass = true;
        }
        return isPass;
    }
    
    public static void main(String[] args) {
        
    }
    
}
