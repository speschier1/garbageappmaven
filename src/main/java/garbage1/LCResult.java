/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package garbage1;

/**
 *
 * @author speschier
 */
public class LCResult {
    private String serial;
    private String i2Value;

    public LCResult(String serial, String i2Value) {
        this.serial = serial;
        this.i2Value = i2Value;
    }

    
    
    /**
     * @return the serial
     */
    public String getSerial() {
        return serial;
    }

    /**
     * @param serial the serial to set
     */
    public void setSerial(String serial) {
        this.serial = serial;
    }

    /**
     * @return the i2Value
     */
    public String getI2Value() {
        return i2Value;
    }

    /**
     * @param i2Value the i2Value to set
     */
    public void setI2Value(String i2Value) {
        this.i2Value = i2Value;
    }
}
