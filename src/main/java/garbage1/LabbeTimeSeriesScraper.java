/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package garbage1;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author speschier
 */
public class LabbeTimeSeriesScraper {

    CSVWriter writer = null;

    public LabbeTimeSeriesScraper() throws IOException {
        String directory = "\\\\wmesmb.idexxi.com\\manufacturing\\prod\\AREP\\CAT1";
        //String directory = "\\\\wmesmb.idexxi.com\\manufacturing\\prod\\AREP\\CTDX";
        String instrument = "CAT1";
        final long startTime = System.currentTimeMillis();
        try {
            writer = new CSVWriter(new FileWriter(instrument + "_TIMESERIES.csv"), ',');
        } catch (IOException ex) {
            Logger.getLogger(LabbeTimeSeriesScraper.class.getName()).log(Level.SEVERE, null, ex);
        }
        doThings(directory, instrument);
        if (writer != null) {
            writer.close();
        }
        System.out.println(String.format(
                "Application run time in %s minutes.",
                TimeUnit.MILLISECONDS.toMinutes(System.currentTimeMillis() - startTime)));
    }

    private void doThings(String directory, String instrument) throws IOException {
        Path dirs = Paths.get(directory);

        Files.walkFileTree(dirs, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult preVisitDirectory(Path file,
                    BasicFileAttributes attrs) {

                DirectoryStream.Filter<Path> filter = new DirectoryStream.Filter<Path>() {
                    @Override
                    public boolean accept(Path entry) throws IOException {
                        BasicFileAttributes attr = Files.readAttributes(entry,
                                BasicFileAttributes.class);
                        LocalDateTime creationTime = LocalDateTime.ofInstant(attr.creationTime().toInstant(), ZoneId.systemDefault());
                        long months = Duration.between(creationTime, LocalDateTime.now()).toDays();
                        return (Math.abs(months) <= 365 && entry.getFileName().toFile().getName().contains("TIMESERIES"));
                    }
                };

                try (DirectoryStream<Path> stream = Files.newDirectoryStream(file, filter)) {

                    for (Path path : stream) {
                        CSVReader reader = new CSVReader(new FileReader(path.toString()), ',', '\'', 1);
                        List<String[]> myEntries = reader.readAll();

                        //timeseries pt = 4 look for 0
                        //assay = 8 look for 42, 41, 40
                        int timeseriesLocation = 0;
                        int assayLocation = 0;

                        if (instrument.equals("CAT1")) {
                            timeseriesLocation = 4;
                            assayLocation = 8;
                        } else if (instrument.equals("CTDX")) {
                            timeseriesLocation = 3;
                            assayLocation = 6;
                        } else {
                            System.out.println("you goober you didnt put in CAT1 or CTDX");
                            System.exit(69);
                        }

                        for (String[] myEntry : myEntries) {
                            if (myEntry[timeseriesLocation].equals("0") && (myEntry[assayLocation].equals("40")
                                    || myEntry[assayLocation].equals("41")
                                    || myEntry[assayLocation].equals("42"))) {
                                String[] writeEntry = Arrays.copyOf(myEntry, myEntry.length + 1);
                                writeEntry[writeEntry.length - 1] = path.toString();
                                writer.writeNext(writeEntry);
                            }
                        }
                    }

                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                return FileVisitResult.CONTINUE;

            }
        });
    }

    public static void main(String[] args) {
        try {
            new LabbeTimeSeriesScraper();
        } catch (IOException ex) {
            Logger.getLogger(LabbeTimeSeriesScraper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
