/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package garbage1;

import au.com.bytecode.opencsv.CSVWriter;
import com.idexx.opsys.hematology.lasercyte.cadillac.CadillacParam;
import com.idexx.opsys.hematology.model.LaserCyteFCSFile;
import com.idexx.opsys.hematology.utils.CadillacConstants;
import com.idexx.utils.StringUtils;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

/**
 *
 * @author speschier
 */
public class LaserCyteFCSCrawler {

    private static final List<String> PATHS = new ArrayList<>();
    //private static final String PATH = "C:\\Users\\speschier\\Desktop\\1541";
    private static final List<String> parameters = new ArrayList<>();
    private String OUTPUT_FILE = "C:\\Users\\speschier\\AJ_FCS_DATA_53.csv";
    private CSVWriter writer = null;
    private Date startDate;
    private Date endDate;

    public LaserCyteFCSCrawler() throws IOException, ParseException {

        PATHS.add("\\\\wmesmb.idexxi.com\\production\\Active\\Lasercyte-Production");

        String startDateString = "01/26/2021";
        startDate = new SimpleDateFormat("MM/dd/yyyy").parse(startDateString);
        String endDateString = "05/04/2021";
        endDate = new SimpleDateFormat("MM/dd/yyyy").parse(endDateString);

        parameters.add("Filename");
        parameters.add("IDXRunDateTime");
        parameters.add("IDXResult-WBC");
        parameters.add("IDXResult-NEU");
        parameters.add("IDXResult-LYM");
        parameters.add("IDXResult-MONO");
        parameters.add("IDXResult-EOS");
        parameters.add("IDXResult-BASO");
        parameters.add("IDXResult-RBC");
        parameters.add("IDXResult-HGB");
        parameters.add("IDXResult-HCT");
        parameters.add("IDXResult-MCV");
        parameters.add("IDXResult-MCH");
        parameters.add("IDXResult-MCHC");
        parameters.add("IDXResult-RDW");
        parameters.add("IDXResult-PLT");
        parameters.add("IDXResult-MPV");
        parameters.add("IDXFlags");
        parameters.add("IDXResult-#RED COUNTED EVENTS");
        parameters.add("IDXResult-#RED DIGITIZED EVENTS");
        parameters.add("IDXResult-#RBC");
        parameters.add("IDXResult-%DOUBLET");
        parameters.add("IDXResult-%RBC_FRAG");
        parameters.add("IDXSpecies");
        parameters.add("IDXResult-RED COUNT RATE FIT SLOPE");
        parameters.add("RBCSheathVol2");
        parameters.add("RBCSheathVol");
        parameters.add("IDXResult-RBC TOF PEAK MEAN");
        parameters.add("IDXResult-WHITE LATEX TOF MEAN");
        parameters.add("IDXResult-RED LATEX TOF MEAN");
        parameters.add("RedsEXTi2");
        parameters.add("RedsRASi2");
        parameters.add("RedsFSLi2");
        parameters.add("RedsFSHi2");
        parameters.add("WhitesEXTi2");
        parameters.add("WhitesRASi2");
        parameters.add("WhitesFSLi2");
        parameters.add("WhitesFSHi2");
        parameters.add("IDXMCVGain");
        parameters.add("IDXRBCGain");
        parameters.add("IDXHGBGain");
        parameters.add("IDXWhiteCorrHGBGain");
        parameters.add("IDXClassifierMap");
        parameters.add("IDXFlags");
        parameters.add("IDXUserFlags");
        parameters.add("IDXSampleID");
        parameters.add("FCSFileCreatedBySWVersion");
        parameters.add("IDXClientID");
        parameters.add("IDXCBC5RBarcode");
        parameters.add("IDXResult-RED %LATEX RECOVERED");
        parameters.add("IDXResult-WHITE %LATEX RECOVERED");
        parameters.add("IDXResult-RETIC");
        parameters.add("IDXResult-%RETIC");
        parameters.add("IDXResult-#WHITE R7");
        parameters.add("IDXResult-#WHITE R8");
        parameters.add("IDXResult-RED HGB");
        parameters.add("IDXResult-WHITE HGB BLUE");
        parameters.add("IDXResult-RED HGB BLUE");
        parameters.add("IDXResult-#RED LATEX_ORIGINAL");
        parameters.add("IDXResult-#WHITE LATEX_ORIGINAL");
        parameters.add("IDXResult-#RED LATEX_AGG");
        parameters.add("IDXResult-#WHITE LATEX_AGG");
        parameters.add("IDXResult-%RED LATEX_AGG of FCS");
        parameters.add("IDXResult-%WHITE LATEX_AGG of FCS");
        parameters.add("IDXResult-RBC FSL PEAK MEAN");
        parameters.add("IDXResult-RED LATEX FSL PEAK MEAN");
        parameters.add("IDXResult-RBC FSH PEAK MEAN");
        parameters.add("IDXResult-RED LATEX FSH PEAK MEAN");
        parameters.add("IDXResult-RBC RAS PEAK MEAN");
        parameters.add("IDXResult-RED LATEX RAS PEAK MEAN");
        parameters.add("IDXResult-RBC EXT PEAK MEAN");
        parameters.add("IDXResult-RED LATEX EXT PEAK MEAN");
        parameters.add("IDXResult-RBC TOF PEAK MEAN");
        parameters.add("IDXResult-RED LATEX TOF MEAN");
        parameters.add("IDXResult-WHITE NEU CEN FSL PEAK");
        parameters.add("IDXResult-WHITE LATEX FSL PEAK MEAN");
        parameters.add("IDXResult-WHITE LATEX FSH PEAK MEAN");
        parameters.add("IDXResult-WHITE NEU CEN RAS PEAK");
        parameters.add("IDXResult-WHITE LATEX RAS PEAK MEAN");
        parameters.add("IDXResult-WHITE NEU CEN EXT PEAK");
        parameters.add("IDXResult-WHITE LATEX EXT PEAK MEAN");
        parameters.add("IDXResult-WHITE NEU TOF MEAN");
        parameters.add("IDXResult-WHITE LATEX TOF MEAN");
        parameters.add("IDXResult-WHITE SHEATH BLUE AVG");
        parameters.add("IDXResult-WHITE SHEATH GREEN AVG");
        parameters.add("IDXResult-WHITE SHEATH RED AVG");
        parameters.add("IDXResult-WHITE SHEATH YELLOW AVG");
        parameters.add("IDXResult-RED SHEATH BLUE AVG");
        parameters.add("IDXResult-RED SHEATH GREEN AVG");
        parameters.add("IDXResult-RED SHEATH RED AVG");
        parameters.add("IDXResult-RED SHEATH YELLOW AVG");
        parameters.add("IDXInitialVialBlockTemp");
        parameters.add("IDXRedVialBlockTemp");
        parameters.add("IDXWhiteVialBlockTemp");

        File file = new File(OUTPUT_FILE);
        if (file.exists()) {
            file.delete();
        }

        writer = new CSVWriter(new FileWriter(OUTPUT_FILE), ',');

        String header = "";
        for (String parameter : parameters) {
            header += parameter + ",";
        }
        header += "File_Path";
        writer.writeNext(header.split(","));
        for (String path : PATHS) {
            try (Stream<Path> paths = Files.walk(Paths.get(path))) {
                paths
                        .filter(Files::isRegularFile)
                        .filter(p -> new Date(p.toFile().lastModified()).after(startDate))
                        .filter(p -> new Date(p.toFile().lastModified()).before(endDate))
                        .filter(p -> p.toString().endsWith("White.FCS"))
                        .forEach(this::processFile);
            } catch (IOException ex) {
                Logger.getLogger(CSVMonitorCrawler.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        writer.close();

    }

    private void processFile(Path file) {
        System.out.println(file);
        try {
            LaserCyteFCSFile lcFile = new LaserCyteFCSFile(file.toFile());
            String row = "";
            for (String parameter : parameters) {
                if (parameter.contains("Flags")) {
                    row += lcFile.getParameter(parameter) != null ? lcFile.getParameter(parameter).replace(",", "|") : "";
                } else {
                    row += lcFile.getParameter(parameter) != null ? lcFile.getParameter(parameter).split(",")[0] : "";
                }
                row += ",";
            }
            row += file.toString();
            writer.writeNext(row.split(","));
        } catch (IOException ex) {
            Logger.getLogger(LaserCyteFCSCrawler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void main(String[] args) {
        try {
            new LaserCyteFCSCrawler();
        } catch (IOException ex) {
            Logger.getLogger(LaserCyteFCSCrawler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(LaserCyteFCSCrawler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
