/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package garbage1;

import au.com.bytecode.opencsv.CSVWriter;
import com.idexx.opsys.hematology.model.LaserCyteFCSFile;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

/**
 *
 * @author speschier
 */
public class LaserCyteFCSCrawlerConditional {

    private static final String PATH = "\\\\orono\\production\\Active\\Lasercyte-Production";
    //private static final String PATH = "C:\\Users\\speschier\\Desktop\\1541";
    private static final List<String> parameters = new ArrayList<>();
    private String OUTPUT_FILE = "C:\\Users\\speschier\\Desktop\\SHAWN_DATA.csv";
    private CSVWriter writer = null;
    private Date inputDate;

    public LaserCyteFCSCrawlerConditional() throws IOException, ParseException {
        parameters.add("IDXResult-#RED LATEX_ORIGINAL");
        parameters.add("IDXResult-#WHITE LATEX");
        String inputString = "08/01/2017";
        inputDate = new SimpleDateFormat("MM/dd/yyyy").parse(inputString);

        try (Stream<Path> paths = Files.walk(Paths.get(PATH))) {
            paths
                    .filter(Files::isRegularFile)
                    .filter(p -> new Date(p.toFile().lastModified()).after(inputDate))
                    .filter(p -> p.toString().endsWith("White.FCS"))
                    .forEach(this::processFile);
        } catch (IOException ex) {
            Logger.getLogger(CSVMonitorCrawler.class.getName()).log(Level.SEVERE, null, ex);
        }
        writer.close();

    }

    private void processFile(Path file) {
        try {
            LaserCyteFCSFile lcFile = new LaserCyteFCSFile(file.toFile());
            Double redValue = lcFile.getParameter("IDXResult-#RED LATEX_ORIGINAL") != null ? Double.parseDouble(lcFile.getParameter("IDXResult-#RED LATEX_ORIGINAL").split(",")[0]) : null;
            Double whiteValue = lcFile.getParameter("IDXResult-#WHITE LATEX") != null ? Double.parseDouble(lcFile.getParameter("IDXResult-#WHITE LATEX").split(",")[0]) : null;
            if (redValue == null || redValue <= 350) {
                System.out.println(file + " - IDXResult-#RED LATEX_ORIGINAL is " + (redValue != null ? redValue : "null"));
            }
            if (whiteValue == null || whiteValue <= 1000) {
                System.out.println(file + " - IDXResult-#WHITE LATEX is " + (whiteValue != null ? whiteValue : "null"));
            }
            
        } catch (IOException ex) {
            Logger.getLogger(LaserCyteFCSCrawlerConditional.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void main(String[] args) {
        try {
            new LaserCyteFCSCrawlerConditional();
        } catch (IOException ex) {
            Logger.getLogger(LaserCyteFCSCrawlerConditional.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(LaserCyteFCSCrawlerConditional.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
