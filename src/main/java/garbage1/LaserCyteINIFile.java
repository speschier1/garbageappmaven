package garbage1;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author speschier
 */
public class LaserCyteINIFile {
    private Date runDate;
    private String serialNumber;
    private String header;
    private Map<String, String> parameterMap;

    public LaserCyteINIFile() {
        parameterMap = new HashMap<>();
    }

    @Override
    public String toString() {
        return "LaserCyteINIFile{" + "runDate=" + runDate + ", serialNumber=" + serialNumber + ", header=" + header + ", parameterMap=" + parameterMap + '}';
    }

    /**
     * @return the runDate
     */
    public Date getRunDate() {
        return runDate;
    }

    /**
     * @param runDate the runDate to set
     */
    public void setRunDate(Date runDate) {
        this.runDate = runDate;
    }

    /**
     * @return the serialNumber
     */
    public String getSerialNumber() {
        return serialNumber;
    }

    /**
     * @param serialNumber the serialNumber to set
     */
    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    /**
     * @return the header
     */
    public String getHeader() {
        return header;
    }

    /**
     * @param header the header to set
     */
    public void setHeader(String header) {
        this.header = header;
    }

    /**
     * @return the parameterMap
     */
    public Map<String, String> getParameterMap() {
        return parameterMap;
    }

    /**
     * @param parameterMap the parameterMap to set
     */
    public void setParameterMap(Map<String, String> parameterMap) {
        this.parameterMap = parameterMap;
    }
    
    
    
}
