/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package garbage1;

import com.idexx.utils.model.ADSAuthenticator;
import com.idexx.utils.model.Employee;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.NamingException;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

/**
 *
 * @author speschier
 */
public class MEUserChecker {

    public MEUserChecker() throws FileNotFoundException, IOException, NamingException {
        File outputFile = new File("c:\\users\\speschier\\desktop\\Book2.xls");
        ADSAuthenticator ads = new ADSAuthenticator();
        if (outputFile.exists()) {
            outputFile.delete();
        }
        FileInputStream file = new FileInputStream(new File("c:\\users\\speschier\\desktop\\Book1.xls"));
        HSSFWorkbook workbook = new HSSFWorkbook(file);
        HSSFSheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.iterator();
        rowIterator.next();
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            for (int i = 0; i < 1; i++) {
                String user = row.getCell(i).toString();
                Optional<Employee> emp = ads.getEmployee(user);
                if (emp == null) {
                    Cell cell = row.createCell(i + 1);
                    cell.setCellValue("X");
                }
            }
        }
        file.close();
        FileOutputStream out
                = new FileOutputStream(outputFile);
        workbook.write(out);
        out.close();

    }

    public static void main(String[] args) {
        try {
            new MEUserChecker();
        } catch (IOException ex) {
            Logger.getLogger(MEUserChecker.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NamingException ex) {
            Logger.getLogger(MEUserChecker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
