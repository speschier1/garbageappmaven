/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package garbage1;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Base64;

/**
 *
 * @author speschier
 */
public class MIIReport {
    private static final String ENCODING = "UTF-8";

    /**
     * @param args
     */
    public static void main(String[] args) {
        try {
            URL url = new URL ("http://scsmhp.idexxi.com:50000/XMII/Illuminator?QueryTemplate=DeviceHistoryReport/DeviceReport/XQ_Assembly&Content-Type=text/json&Session=false&Param.1=CTDX012345&t=1575919432991&Param.2=CURRENT");
            String encoding = Base64.getEncoder().encodeToString("wsuser:idexx123".getBytes(ENCODING));
            
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setRequestProperty  ("Authorization", "Basic " + encoding);
            InputStream content = (InputStream)connection.getInputStream();
            BufferedReader in   = 
                new BufferedReader (new InputStreamReader (content));
            String line;
            while ((line = in.readLine()) != null) {
                System.out.println(line);
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}
