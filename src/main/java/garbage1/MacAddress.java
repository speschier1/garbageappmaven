/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package garbage1;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;

/**
 *
 * @author speschier
 */
public class MacAddress {

    public static void main(String[] args) throws UnknownHostException {
        long sequenceValue = 38353411800L;
        for (long i = sequenceValue; i < 38353411808L; i++) {
            byte[] mac = ByteBuffer.allocate(Long.SIZE / Byte.SIZE).putLong(i).array();

            StringBuilder sb = new StringBuilder();
            for (int x = 2; x < mac.length; x++) {
                sb.append(String.format("%02X", mac[x]));
            }

            System.out.println(i + " = " + sb);
        }

    }
}
