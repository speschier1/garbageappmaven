///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package garbage1;
//
//import au.com.bytecode.opencsv.CSVReader;
//import com.idexx.sap.me.sapmelib.common.DBEnum;
//import com.idexx.sap.me.sapmelib.exceptions.SAPMELibException;
//import com.sap.me.production.ChangeProductionContext;
//import com.sap.me.production.ChangeProductionRequest;
//import com.sap.me.production.ChangeProductionResponse;
//import java.io.File;
//import java.io.FileNotFoundException;
//import java.io.FileReader;
//import java.io.IOException;
//import java.util.List;
//import java.util.Map;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import javax.xml.ws.BindingProvider;
//import mepapi.com.sap.me.production.ChangeProductionFault;
//import mepapi.com.sap.me.production.ChangeProductionServiceWS;
//import mepapi.com.sap.me.production.ChangeProductionServiceWSService;
//
///**
// *
// * @author speschier
// */
//public class MaterialUpdater {
//
//    private static final String CSV_PATH = "c:\\users\\speschier\\desktop\\WIP - NEW GF MATERIAL TO CHANGE IN SAP MEP.csv";
//    private static final DBEnum database = DBEnum.QA;
//
//    public MaterialUpdater() throws FileNotFoundException, IOException, SAPMELibException, ChangeProductionFault {
//
//        try (CSVReader reader = new CSVReader(new FileReader(new File(CSV_PATH)), ',', '\"', 1)) {
//            List<String[]> data = reader.readAll();
//            for (String[] strings : data) {
//                String sfc = strings[0].trim();
//                String material = strings[1].trim();
//                System.out.println(String.format("Updating %s to %s", sfc, material));
//                ChangeProductionRequest request = new ChangeProductionRequest();
//                ChangeProductionContext context = new ChangeProductionContext();
//                context.setNewItem(material);
//                context.setNewItemRevision("#");
//                request.setChangeProductionContextType(context);
//                request.getSfcList().add(sfc);
//                //changeProduction("USPB",request);
//            }
//        }
//    }
//
//    private ChangeProductionResponse changeProduction(String site, ChangeProductionRequest request) throws ChangeProductionFault {
//        ChangeProductionServiceWSService service = new ChangeProductionServiceWSService();
//        ChangeProductionServiceWS port = service.getChangeProductionServiceWSPort();
//        Map<String, Object> context = ((BindingProvider) port).getRequestContext();
//        context.put(BindingProvider.USERNAME_PROPERTY, Authentication.getWSUsername());
//        context.put(BindingProvider.PASSWORD_PROPERTY, Authentication.getWSPassword());
//        context.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, Authentication.getWSUrl(context.get(BindingProvider.ENDPOINT_ADDRESS_PROPERTY).toString()));
//        System.out.println(String.format("changeProduction WSDL Address: %s", context.get(BindingProvider.ENDPOINT_ADDRESS_PROPERTY)));
//        return port.changeProduction(site, "", request);
//    }
//
//    public static void main(String[] args) {
//        try {
//            new MaterialUpdater();
//        } catch (IOException | SAPMELibException | ChangeProductionFault ex) {
//            Logger.getLogger(MaterialUpdater.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
//
//}
