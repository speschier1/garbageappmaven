/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package garbage1;

import au.com.bytecode.opencsv.CSVWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

/**
 *
 * @author speschier
 */
public final class MikeMorinIQAPDXTicket13941 {

    List<File> datOnly = new ArrayList<>();

    public MikeMorinIQAPDXTicket13941() throws IOException, InvalidFormatException {

        //final String BASE_DIR = "\\\\FOGHORN\\Groups\\IQA\\ProCyte Dx\\Calibration Spreadsheet";
        final String BASE_DIR = "C:\\Users\\speschier\\Desktop\\Calibration Spreadsheet";
        datOnly.clear();
        List<File> files = searchForDatFiles(new File(BASE_DIR));

        System.out.println("Found " + files.size() + " files, beginning parse");

        CSVWriter writer = null;
        try {
            
            File file = new File("IQA_PDX_DATA.csv");
            if(file.exists()) {
                file.delete();
            }
            
            writer = new CSVWriter(new FileWriter("IQA_PDX_DATA.csv"), ',');
        } catch (IOException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
            return;
        }
        String header = "Instrument ID, Date,Time,Sample #,RBC,HGB,HCT,MCV,MCH,MCHC,RDW-SD,RDW-CV,RET #,RET%,RBC-O,PLT-O,RBC-X,RBC-Y,RBC-Z,PDW,MPV,P-LCR,PCT,PLT,WBC,NEUT#,LYMPH#,MONO#,EO#,BASO#,DIFF-X,DIFF-Y,FSC-X,HGB Blank,ASP Blank";
        writer.writeNext(header.split(","));
        int fileCount = 0;
        List<String> rowStringList;
        for (File file : files) {
            if (file.exists() && file.canRead()) {
                System.out.println("Processing: " + file.getAbsolutePath());
                Workbook wb = WorkbookFactory.create(file);
                Sheet sheet = wb.getSheet("Low Control");
                if (sheet != null) {

                    Row rowCheck = sheet.getRow(4);
                    if (rowCheck != null) {
                        Cell cellCheck = rowCheck.getCell(1);
                        if (cellCheck != null) {
                            System.out.println("DATE CELL CHECK - '" + cellCheck.getStringCellValue().trim().toUpperCase() + "'");
                            if (!cellCheck.getStringCellValue().trim().toUpperCase().contains("DATE")) {
                                continue;
                            } 
                        } else {
                            continue;
                        }
                    } else {
                        continue;
                    }
                    rowCheck = sheet.getRow(6);
                    if (rowCheck != null) {
                        Cell cellCheck = rowCheck.getCell(0);
                        if (cellCheck != null) {
                            System.out.println("EMPTY CELL CHECK - '" + cellCheck.getStringCellValue() + "'");
                            if (cellCheck.getStringCellValue().isEmpty()) {
                                continue;
                            } else {
                                fileCount++;
                            }
                        } else {
                            continue;
                        }
                    } else {
                        continue;
                    }

                    for (int rowCount = 5; rowCount < 15; rowCount++) {
                        rowStringList = new ArrayList<>();
                        for (int col = 0; col < 194; col++) {
                            if (!sheet.isColumnHidden(col)) {
                                Row row = sheet.getRow(rowCount);
                                if (row != null) {
                                    Cell cell = row.getCell(col);
                                    if (cell != null) {
                                        switch (cell.getCellType()) {
                                            case Cell.CELL_TYPE_STRING:
                                                rowStringList.add(cell.getRichStringCellValue().getString());
                                                break;
                                            case Cell.CELL_TYPE_NUMERIC:
                                                if (DateUtil.isCellDateFormatted(cell)) {
                                                    rowStringList.add(cell.getDateCellValue() + "");
                                                } else {
                                                    rowStringList.add(cell.getNumericCellValue() + "");
                                                }
                                                break;
                                            case Cell.CELL_TYPE_BOOLEAN:
                                                rowStringList.add(cell.getBooleanCellValue() + "");
                                                break;
                                            case Cell.CELL_TYPE_FORMULA:
                                                switch (cell.getCachedFormulaResultType()) {
                                                    case Cell.CELL_TYPE_NUMERIC:
                                                        rowStringList.add(cell.getNumericCellValue() + "");
                                                        break;
                                                    case Cell.CELL_TYPE_STRING:
                                                        rowStringList.add(cell.getRichStringCellValue() + "");
                                                        break;
                                                }
                                                break;
                                            default:
                                                rowStringList.add("");
                                        }
                                    }
                                }
                            }
                        }
                        String[] entries = rowStringList.toArray(new String[0]);
                        writer.writeNext(entries);
                    }
                }
            }
            System.out.println(String.format("Processed %s of %s ", fileCount, files.size()));
        }
        writer.close();
    }

    public List<File> searchForDatFiles(File root) {
        final Pattern PROCYTE_SN = Pattern.compile("[A-Z]\\d{4,6}.xlsm|[A-Z]\\d{4,6}.xls", Pattern.CASE_INSENSITIVE);

        if (root.isDirectory()) {
            for (File file : root.listFiles()) {
                searchForDatFiles(file);
            }
        } else if (root.isFile() && PROCYTE_SN.matcher(root.getName()).find() && !root.getName().contains("~")) {
            datOnly.add(root);
        }
        return datOnly;
    }

    public static void main(String[] args) {
        try {
            new MikeMorinIQAPDXTicket13941();
        } catch (IOException ex) {
            Logger.getLogger(MikeMorinIQAPDXTicket13941.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidFormatException ex) {
            Logger.getLogger(MikeMorinIQAPDXTicket13941.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
