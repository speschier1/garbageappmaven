/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package garbage1;

import com.idexx.utils.model.NetworkFileCopier;
import java.io.IOException;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 *
 * @author speschier
 */
import java.security.GeneralSecurityException;

public class Mover {
//    public static void main(String... args)
//    {
//        executeAsAdministrator("c:\\windows\\system32\\notepad.exe", "");
//    }
//
//    public static void executeAsAdministrator(String command, String args)
//    {
//        Shell32X.SHELLEXECUTEINFO execInfo = new Shell32X.SHELLEXECUTEINFO();
//        execInfo.lpFile = new WString(command);
//        if (args != null)
//            execInfo.lpParameters = new WString(args);
//        execInfo.nShow = Shell32X.SW_SHOWDEFAULT;
//        execInfo.fMask = Shell32X.SEE_MASK_NOCLOSEPROCESS;
//        execInfo.lpVerb = new WString("runas");
//        boolean result = Shell32X.INSTANCE.ShellExecuteEx(execInfo);
//
//        if (!result)
//        {
//            int lastError = Kernel32.INSTANCE.GetLastError();
//            String errorMessage = Kernel32Util.formatMessageFromLastErrorCode(lastError);
//            throw new RuntimeException("Error performing elevation: " + lastError + ": " + errorMessage + " (apperror=" + execInfo.hInstApp + ")");
//        }
//    }

    public static void main(String[] args) throws GeneralSecurityException, IOException {
        NetworkFileCopier networkFileCopier;

        String redSource = "C:\\Program Files\\IDEXX\\VetStation\\rawfiles\\DXBP001164_190202_094918_Red.FCS";
        String whiteSource = "C:\\Program Files\\IDEXX\\VetStation\\rawfiles\\DXBP001164_190202_094918_White.FCS";

        String redTarget = "smb://wmesmb.idexxi.com/production/Active/Lasercyte-Development/DXBP001164_190202_094918_Red.FCS";
        String whiteTarget = "smb://wmesmb.idexxi.com/production/Active/Lasercyte-Development/DXBP001164_190202_094918_White.FCS";

//        networkFileCopier = new NetworkFileCopier();
//        networkFileCopier.createCopyOnNetwork(redSource, redTarget);
//        networkFileCopier.createCopyOnNetwork(whiteSource, whiteTarget);
        Path redPath = Paths.get(redSource);
        Path whitePath = Paths.get(whiteSource);

        try {
            Files.delete(redPath);
        } catch (NoSuchFileException x) {
            System.err.format("%s: no such" + " file or directory%n", redPath);
        } catch (DirectoryNotEmptyException x) {
            System.err.format("%s not empty%n", redPath);
        } catch (IOException x) {
            // File permission problems are caught here.
            System.err.println(x);
        }

    }
}
