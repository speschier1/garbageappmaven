/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package garbage1;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class NewSingleThreadExecutorTest {

    public static void main(final String... args) {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        Callable task = new Callable<Object>() {
            @Override
            public Object call() {
                throw new RuntimeException("foo");
            }
        };

        Future<?> future = executor.submit(task);
        try {
            future.get();
        } catch (ExecutionException | InterruptedException e) {
            
        }
    }

}
