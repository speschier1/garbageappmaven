package garbage1;

import com.idexx.opsys.hematology.common.HemaLibDBEnum;
import com.idexx.opsys.hematology.common.HemaLibException;
import com.idexx.opsys.hematology.lasercyte.bloodlab.RunType;
import com.idexx.opsys.hematology.lasercyte.cadillac.CadillacDAO;
import com.idexx.opsys.hematology.lasercyte.cadillac.CadillacPopulationData;
import com.idexx.opsys.hematology.lasercyte.cadillac.CadillacRun;
import com.idexx.opsys.hematology.lasercyte.cadillac.CadillacRunGroup;
import com.idexx.opsys.hematology.lasercyte.cadillac.PopulationDataList;
import com.idexx.opsys.hematology.lasercyte.calc.CalcEngine;
import com.idexx.opsys.hematology.lasercyte.config.Operation;
import com.idexx.opsys.hematology.model.FCSParameters;
import com.idexx.opsys.hematology.utils.CadillacConstants;
import com.idexx.utils.NotificationUtils;
import com.idexx.utils.view.DetailedInfoPanel;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PopulationDataBuilder {

    private static final Logger LOGGER = LoggerFactory.getLogger(PopulationDataBuilder.class);

    private static final HemaLibDBEnum DATABASE = HemaLibDBEnum.PRODUCTION;

    PopulationDataList dataList = new PopulationDataList();

    public PopulationDataBuilder() {
        dataList = new PopulationDataList();
    }

    public void savePopulationData(CadillacRunGroup runGroup, CalcEngine calcEngine, List<RunType> runTypes, Operation operation) throws HemaLibException {
        dataList.getPopDataList().clear();
        Set<String> wbRunIdList = new HashSet<>();
        boolean isQCRuns = false;
        for (RunType runType : runTypes) {
            for (CadillacRun run : runGroup.getValidRuns()) {
                for (FCSParameters parameter : FCSParameters.values()) {
                    if (run.getTarget().getRunType().equals(runType)) {
                        switch (operation.getRunType()) {
                            case WB:
                                //System.out.println("WB FOR RUN " + run.getCadillacRunId() + " PAram =" + parameter);
                                doWholeBloodPopData(run, runType, parameter);
                                break;
                            case QC1:
                            case QC3:
                                //System.out.println("QC FOR RUN " + run.getCadillacRunId() + " PAram =" + parameter);
                                isQCRuns = true;
                                doAccuracyPopData(run, parameter, calcEngine);
                                break;
                            case FRESH:
                                //System.out.println("FRESH FOR RUN " + run.getCadillacRunId() + " PAram =" + parameter);
                                doFreshPopData(run, parameter);
                                break;
                        }
                    }

                }
            }
        }

        if (isQCRuns) {
            for (CadillacRun run : runGroup.getValidRuns()) {
                if (run.getTarget().getRunType().equals(RunType.WB)) {
                    wbRunIdList.add(run.getCadillacRunId() + "");
                }
            }
            doGetWBValuesAndAccuracyApplySlope(wbRunIdList, calcEngine);
        }
        try {
            new CadillacDAO().savePopData(dataList, DATABASE);
        } catch (HemaLibException ex) {
            LOGGER.error("Pop Data: " + ex.getMessage(), ex);
            //NotificationUtils.showError(new DetailedInfoPanel(ex));
        }
    }

    //just the result IDXResult-* value
    private void doFreshPopData(CadillacRun run, FCSParameters parameter) {
        CadillacPopulationData popData = new CadillacPopulationData();
        popData.setParamName(parameter.getNameInResult());
        popData.setSlopeApplied(true);
        double calcValue = Double.parseDouble(run.getParameterByName(CadillacConstants.getResultString(parameter)).getValue().split(",")[0]);
        popData.setParamValue(CadillacConstants.round(calcValue, 3));
        popData.setTargetID(run.getTarget().getTargetID());
        popData.setRunId(run.getCadillacRunId());
        dataList.getPopDataList().add(popData);
    }

    //this is done on Accuracy submit to apply the slopes from accuracy to whole blood values.
    private void doGetWBValuesAndAccuracyApplySlope(Set<String> wbRunIdList, CalcEngine calcEngine) throws HemaLibException {
        final CadillacDAO cadillacDAO = new CadillacDAO();
        List<CadillacPopulationData> wbPopData = cadillacDAO.getPopData(wbRunIdList, DATABASE);
        for (int i = 0; i < wbPopData.size(); i++) {
            FCSParameters parameter = FCSParameters.fromConfig(wbPopData.get(i).getParamName());
            double calcValue;
            if (parameter.isDisplayCalFactor()) {
                if (parameter.equals(FCSParameters.PLT)) {
                    double slopeRBC = calcEngine.getRobustSlope(parameter.getNameInResult());
                    double slopePLT = calcEngine.getRobustSlope(FCSParameters.RBC.getNameInResult());
                    calcValue = wbPopData.get(i).getParamValue() * (slopeRBC * slopePLT);
                    wbPopData.get(i).setParamValue(CadillacConstants.round(calcValue, 3));
                } else {
                    double slope = calcEngine.getRobustSlope(parameter.getNameInResult());
                    calcValue = wbPopData.get(i).getParamValue() * slope;
                    wbPopData.get(i).setParamValue(CadillacConstants.round(calcValue, 3));
                }
            }
            wbPopData.get(i).setSlopeApplied(true);
            dataList.getPopDataList().add(wbPopData.get(i));
        }
    }

    private void doAccuracyPopData(CadillacRun run, FCSParameters parameter, CalcEngine calcEngine) {
        CadillacPopulationData popData = new CadillacPopulationData();
        popData.setParamName(parameter.getNameInResult());
        popData.setSlopeApplied(true);
        double calcValue;
        if (!parameter.isDisplayCalFactor()) {
            calcValue = Double.parseDouble(run.getParameterByName(CadillacConstants.getResultString(parameter)).getValue().split(",")[0]);
        } else if (FCSParameters.PLT.equals(parameter)) {
            //(IDXResult-PLT / (RBCGain * PLTGain) ) * (RBC_SLOPE * PLT_SLOPE)  FUCK MY LIFE!!!!
            double resultPLT = Double.parseDouble(run.getParameterByName(CadillacConstants.getResultString(parameter)).getValue().split(",")[0]);
            double gainPLT = Double.parseDouble(run.getParameterByName(CadillacConstants.getGainString(parameter)).getValue());
            double gainRBC = Double.parseDouble(run.getParameterByName(CadillacConstants.getGainString(FCSParameters.RBC)).getValue());
            double slopeRBC = calcEngine.getRobustSlope(FCSParameters.RBC.getNameInResult());
            double slopePLT = calcEngine.getRobustSlope(FCSParameters.PLT.getNameInResult());
            calcValue = (resultPLT / (gainRBC * gainPLT)) * (slopeRBC * slopePLT);
        } else {
            double result = Double.parseDouble(run.getParameterByName(CadillacConstants.getResultString(parameter)).getValue().split(",")[0]);
            double gain = Double.parseDouble(run.getParameterByName(CadillacConstants.getGainString(parameter)).getValue());
            double slope = calcEngine.getRobustSlope(parameter.getNameInResult());
            calcValue = (result / gain) * slope;
        }
        popData.setParamValue(CadillacConstants.round(calcValue, 3));
        popData.setTargetID(run.getTarget().getTargetID());
        popData.setRunId(run.getCadillacRunId());
        dataList.getPopDataList().add(popData);
    }

    private void doWholeBloodPopData(CadillacRun run, RunType runType, FCSParameters parameter) {
        if (run.getTarget().getRunType().equals(runType)) {
            CadillacPopulationData popData = new CadillacPopulationData();
            popData.setParamName(parameter.getNameInResult());
            popData.setSlopeApplied(false);
            double calcValue;
            if (!parameter.isDisplayCalFactor()) {
                calcValue = Double.parseDouble(run.getParameterByName(CadillacConstants.getResultString(parameter)).getValue().split(",")[0]);
            } else if (FCSParameters.PLT.equals(parameter)) {
                //(IDXResult-PLT / (RBCGain * PLTGain) ) * (RBC_SLOPE * PLT_SLOPE)  FUCK MY LIFE!!!!
                double result = Double.parseDouble(run.getParameterByName(CadillacConstants.getResultString(parameter)).getValue().split(",")[0]);
                double gainPLT = Double.parseDouble(run.getParameterByName(CadillacConstants.getGainString(parameter)).getValue());
                double gainRBC = Double.parseDouble(run.getParameterByName(CadillacConstants.getGainString(FCSParameters.RBC)).getValue());
                calcValue = (result / (gainPLT * gainRBC));
            } else {
                double result = Double.parseDouble(run.getParameterByName(CadillacConstants.getResultString(parameter)).getValue().split(",")[0]);
                double gain = Double.parseDouble(run.getParameterByName(CadillacConstants.getGainString(parameter)).getValue());
                calcValue = (result / gain);
            }
            popData.setParamValue(CadillacConstants.round(calcValue, 3));
            popData.setTargetID(run.getTarget().getTargetID());
            popData.setRunId(run.getCadillacRunId());
            dataList.getPopDataList().add(popData);
        }
    }
}
