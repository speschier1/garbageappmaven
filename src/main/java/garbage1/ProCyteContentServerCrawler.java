///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package garbage1;
//
//import au.com.bytecode.opencsv.CSVReader;
//import au.com.bytecode.opencsv.CSVWriter;
//import com.idexx.opssys.contentserver.webservices.controller.ContentServer;
//import com.idexx.opssys.contentserver.webservices.exceptions.ContentServerException;
//import com.idexx.opssys.contentserver.webservices.model.CSDataBaseEnum;
//import com.opentext.livelink.service.docman.Node;
//import java.io.File;
//import java.io.FileReader;
//import java.io.FileWriter;
//import java.io.IOException;
//import java.io.Reader;
//import java.nio.file.Files;
//import java.nio.file.Paths;
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//
///**
// *
// * @author speschier
// */
//public class ProCyteContentServerCrawler {
//
//    ContentServer cs;
//    private CSVWriter writer = null;
//    public static final SimpleDateFormat DISPLAY_DATE_FORMAT = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
//    List<String> serialsFound = new ArrayList<>();
//
//    private String OUTPUT_FILE = "C:\\Users\\speschier\\Desktop\\procyte_cal_crawler_ORG_B.csv";
//    private long BASE_NODE_A = 102279478;
//    private long BASE_NODE_B = 102165530;
//    private long BASE_NODE_C = 108853309;
//
//    public ProCyteContentServerCrawler() throws IOException, Exception {
//
//        File file = new File(OUTPUT_FILE);
////        if (file.exists()) {
////            file.delete();
////        }
//        if (file.exists()) {
//            try (CSVReader reader = new CSVReader(new FileReader(file))) {
//                List<String[]> data = reader.readAll();
//
//                data.forEach((strings) -> {
//                    serialsFound.add(strings[0]);
//                });
//
//            }
//        }
//
//        writer = new CSVWriter(new FileWriter(OUTPUT_FILE, true), ',');
//
//        try {
//            cs = new ContentServer(CSDataBaseEnum.PRODUCTION);
//            traverseFolders(BASE_NODE_B);
//        } catch (ContentServerException ex) {
//            Logger.getLogger(ProCyteContentServerCrawler.class.getName()).log(Level.SEVERE, null, ex);
//            ex.printStackTrace();
//        }
//        writer.close();
//    }
//
//    private void traverseFolders(long folderNodeId) throws ContentServerException, Exception {
//        for (Node node : cs.listNodes(folderNodeId, true)) {
//            if (node.isIsContainer()) {
//                traverseFolders(node.getID());
//            } else {
//                if (node.getName().trim().toUpperCase().endsWith(".CAL") && !serialsFound.contains(node.getName().trim())) {
//                    try {
//                        processFile(cs.getFile(node.getID(), 1, "", "", true), cs.getAttachment(node.getID(), 1).getModifiedDate().toString());
//                    } catch (Exception ex) {
//                        String data = node.getName() + ",BROKEN,";
//                        writer.writeNext(data.split(","));
//                    }
//                }
//            }
//        }
//    }
//
//    private void processFile(File file, String modifiedDate) {
//        System.out.println("processing file " + file.getAbsolutePath());
//        Reader reader = null;
//        try {
//            reader = Files.newBufferedReader(Paths.get(file.getAbsolutePath()));
//            CSVReader csvReader = new CSVReader(reader, '\t');
//            String[] nextRecord;
//            String data;
//            while ((nextRecord = csvReader.readNext()) != null) {
//                if (nextRecord[0].equals("RET_CAL")) {
//                    data = file.getName() + "," + nextRecord[1] + "," + modifiedDate;
//                    System.out.println(data);
//                    writer.writeNext(data.split(","));
//                }
//            }
//
//        } catch (Exception ex) {
//            Logger.getLogger(ProCyteContentServerCrawler.class.getName()).log(Level.SEVERE, null, ex);
//        } finally {
//            try {
//                reader.close();
//            } catch (IOException ex) {
//                Logger.getLogger(ProCyteContentServerCrawler.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
//
//        if (!file.delete()) {
//            System.out.println(file.getAbsoluteFile() + " NOT DELETED");
//        }
//
//    }
//
//    public static void main(String[] args) throws IOException, Exception {
//        new ProCyteContentServerCrawler();
//    }
//}
