/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package garbage1;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author speschier
 */
public class Results {
    private String serialNumber = "";
    private String dateTime = "";
    private String vetrolLot = "";
    private String chemLot = "";
    private List<Runs> runList;

    public Results() {
        runList = new ArrayList<Runs>();
    }

    /**
     * @return the serialNumber
     */
    public String getSerialNumber() {
        return serialNumber;
    }

    /**
     * @param serialNumber the serialNumber to set
     */
    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    /**
     * @return the dateTime
     */
    public String getDateTime() {
        return dateTime;
    }

    /**
     * @param dateTime the dateTime to set
     */
    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    /**
     * @return the vetrolLot
     */
    public String getVetrolLot() {
        return vetrolLot;
    }

    /**
     * @param vetrolLot the vetrolLot to set
     */
    public void setVetrolLot(String vetrolLot) {
        this.vetrolLot = vetrolLot;
    }

    /**
     * @return the chemLot
     */
    public String getChemLot() {
        return chemLot;
    }

    /**
     * @param chemLot the chemLot to set
     */
    public void setChemLot(String chemLot) {
        this.chemLot = chemLot;
    }

    /**
     * @return the runList
     */
    public List<Runs> getRunList() {
        return runList;
    }

    /**
     * @param runList the runList to set
     */
    public void setRunList(List<Runs> runList) {
        this.runList = runList;
    }
    
    
}
