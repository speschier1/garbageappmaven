/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package garbage1;

/**
 *
 * @author speschier
 */
public class Runs {
    private String runName;
    private String runValue;

    /**
     * @return the runName
     */
    public String getRunName() {
        return runName;
    }

    /**
     * @param runName the runName to set
     */
    public void setRunName(String runName) {
        this.runName = runName;
    }

    /**
     * @return the runValue
     */
    public String getRunValue() {
        return runValue;
    }

    /**
     * @param runValue the runValue to set
     */
    public void setRunValue(String runValue) {
        this.runValue = runValue;
    }
}
