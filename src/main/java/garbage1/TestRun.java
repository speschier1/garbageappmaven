/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package garbage1;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author speschier
 */
public class TestRun {

    public static void main(String[] args) {
        List<LaserCyteINIFile> fileList = new ArrayList<>();
        try {
            JsonParser parser = new JsonParser();
            String jsonFile = "C:\\Users\\speschier\\Desktop\\lasercyte.json";
            //String contents = new String(Files.readAllBytes(Paths.get(jsonFile)));
            //DXBP007149
            String content = getDataFromService("115605", "DXBP007149", "10");
            JsonElement jsonElement = parser.parse(content);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
            LaserCyteINIFile iniFile;
            if (jsonElement.isJsonObject()) {
                JsonObject object = jsonElement.getAsJsonObject();
                for (Map.Entry<String, JsonElement> entry : object.entrySet()) {
                    iniFile = new LaserCyteINIFile();
                    iniFile.setRunDate(sdf.parse(entry.getKey()));
                    if (entry.getValue().isJsonObject()) {
                        JsonObject obj = entry.getValue().getAsJsonObject();
                        for (Map.Entry<String, JsonElement> e : obj.entrySet()) {
                            if (e.getKey().equals("ini_header")) {
                                iniFile.setHeader(e.getValue().getAsString());
                                iniFile.setSerialNumber(e.getValue().getAsString().substring(1, 11));
                            } else {
                                iniFile.getParameterMap().put(e.getKey(), e.getValue().getAsString());
                            }
                        }
                    }
                    fileList.add(iniFile);
                }
            }
        } catch (ParseException ex) {
            Logger.getLogger(TestRun.class.getName()).log(Level.SEVERE, null, ex);
        }
        for (LaserCyteINIFile laserCyteINIFile : fileList) {
            System.out.println(laserCyteINIFile);
        }
    }

    private static String getDataFromService(String sapNumber, String serialNumber, String numRuns) {
        String url = "http://dev.isd.idexxi.com/fcs_service/rest/fcs/iniData";
        String jsonString = null;
        Client client = Client.create();
        WebResource webResource = client.resource(url);
        webResource = webResource.queryParam("sap", sapNumber);
        webResource = webResource.queryParam("serial", serialNumber);
        webResource = webResource.queryParam("maxCount", numRuns);
        ClientResponse response = webResource.accept(MediaType.APPLICATION_JSON).get(ClientResponse.class);
        if (response.getStatus() != Response.Status.OK.getStatusCode()) {
            throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
        }
        jsonString = response.getEntity(String.class);

        return jsonString;
    }

}
