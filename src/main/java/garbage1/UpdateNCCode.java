package garbage1;

///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package garbage;
//
//import com.sap.xi.me.Description;
//import com.sap.xi.me.NCCode;
//import com.sap.xi.me.NCCodeDefIn;
//import com.sap.xi.me.NCCodeIn;
//import com.sap.xi.me.NCCodeRef;
//import com.sap.xi.me.SHORTName;
//import com.sap.xi.me.SiteRef;
//import java.util.Map;
//import javax.xml.ws.BindingProvider;
//
///**
// *
// * @author speschier
// */
//public class UpdateNCCode {
//
//    public UpdateNCCode() {
//        updateNcCode();
//    }
//
//    private void updateNcCode() {
//
//        try { // Call Web Service Operation
//            com.sap.xi.me.NCCodeService service = new com.sap.xi.me.NCCodeService();
//            com.sap.xi.me.NCCodeProcessingIn port = service.getNCCodeServicePort();
//            Map<String, Object> context = ((BindingProvider) port).getRequestContext();
//            context.put(BindingProvider.USERNAME_PROPERTY, "wsuser");
//            context.put(BindingProvider.PASSWORD_PROPERTY, "idexx123");
//            com.sap.xi.me.NCCodeUpdateRequestMessageSync ncCodeUpdateRequestSync = new com.sap.xi.me.NCCodeUpdateRequestMessageSync();
//            NCCode ncCode = getNcCode("GUIERROR");
//            if (ncCode != null) {
//                Description desc = new Description();
//                desc.setValue("HELLO");
//                ncCode.setDescription(desc);
//                NCCodeIn ncCodeIn = new NCCodeIn();
//                ncCodeIn.setCustomFieldList(ncCode.getCustomFieldList());
//                ncCodeIn.setDescription(desc);
//                ncCodeIn.setDpmoCategoryRef(ncCode.getDpmoCategoryRef());
//                ncCodeIn.setNcCategory(ncCode.getNcCategory());
//                ncCodeIn.setNcCode(ncCode.getNcCode());
//
//                NCCodeDefIn ncCodeDefIn = new NCCodeDefIn();
//                ncCodeDefIn.setAlarmRef(ncCode.getNcCodeDefinition().getAlarmRef());
//                ncCodeDefIn.setAllowLocalRework(ncCode.getNcCodeDefinition().isAllowLocalRework());
//                ncCodeDefIn.setAllowNoDisposition(ncCode.getNcCodeDefinition().isAllowNoDisposition());
//                ncCodeDefIn.setAssignNcToComponent(ncCode.getNcCodeDefinition().getAssignNcToComponent());
//                ncCodeDefIn.setAutoCloseIncident(ncCode.getNcCodeDefinition().isAutoCloseIncident());
//                ncCodeDefIn.setAutoClosePrimary(ncCode.getNcCodeDefinition().isAutoClosePrimary());
//                ncCodeDefIn.setCanBePrimaryCode(ncCode.getNcCodeDefinition().isCanBePrimaryCode());
//                ncCodeDefIn.setClosureRequired(ncCode.getNcCodeDefinition().isClosureRequired());
//                ncCodeDefIn.setCollectRequiredNcData(ncCode.getNcCodeDefinition().getCollectRequiredNcData());
//                ncCodeDefIn.setDispositionGroupRef(ncCode.getNcCodeDefinition().getDispositionGroupRef());
//                ncCodeDefIn.setErpCatalog(ncCode.getNcCodeDefinition().getErpCatalog());
//                ncCodeDefIn.setErpCode(ncCode.getNcCodeDefinition().getErpCode());
//                ncCodeDefIn.setErpCodeGroup(ncCode.getNcCodeDefinition().getErpCodeGroup());
//                ncCodeDefIn.setErpQnCode(ncCode.getNcCodeDefinition().isErpQnCode());
//                ncCodeDefIn.setMaxNcLimit(ncCode.getNcCodeDefinition().getMaxNcLimit());
//                ncCodeDefIn.setNcCodeOrGroup(ncCode.getNcCodeDefinition().getNcCodeOrGroup());
//                ncCodeDefIn.setNcDataTypeRef(ncCode.getNcCodeDefinition().getNcDataTypeRef());
//                NCCodeDefIn.NcDispositionRouterList thissucks = new NCCodeDefIn.NcDispositionRouterList();
//                //thissucks.getNcDispositionRouter().addAll(ncCode.getNcCodeDefinition().getNcDispositionRouterList().getNcDispositionRouter());
//                ncCodeDefIn.setNcDispositionRouterList(thissucks);
//                NCCodeDefIn.NcSecondaryCodeList thefuckdoesthis = new NCCodeDefIn.NcSecondaryCodeList();
//                //thefuckdoesthis.getNcSecondaryCode().addAll(ncCode.getNcCodeDefinition().getNcSecondaryCodeList().getNcSecondaryCode());
//                ncCodeDefIn.setNcSecondaryCodeList(thefuckdoesthis);
//                ncCodeDefIn.setNcSeverityThreshold(ncCode.getNcCodeDefinition().getNcSeverityThreshold());
//                NCCodeDefIn.NcValidOperationList ihatethis = new NCCodeDefIn.NcValidOperationList();
//                //ihatethis.getNcValidOperation().addAll(ncCode.getNcCodeDefinition().getNcValidOperationList().getNcValidOperation());
//                ncCodeDefIn.setNcValidOperationList(ihatethis);
//                ncCodeDefIn.setPriority(ncCode.getNcCodeDefinition().getPriority());
//                ncCodeDefIn.setSecondaryCodeSpecialInstruction(ncCode.getNcCodeDefinition().getSecondaryCodeSpecialInstruction());
//                ncCodeDefIn.setSecondaryRequiredForClose(ncCode.getNcCodeDefinition().isSecondaryRequiredForClose());
//                ncCodeDefIn.setValidAtAllOperations(ncCode.getNcCodeDefinition().isValidAtAllOperations());
//                ncCodeIn.setNcCodeDefinition(ncCodeDefIn);
//                NCCodeIn.NcGroupMemberList memberList = new NCCodeIn.NcGroupMemberList();
//                //memberList.getNcGroupMember().addAll(ncCode.getNcGroupMemberList().getNcGroupMember());
//                ncCodeIn.setNcGroupMemberList(memberList);
//                SiteRef ref = new SiteRef();
//                ref.setSite("USPB");
//                ncCodeIn.setSiteRef(ref);
//                ncCodeIn.setStatusRef(ncCode.getStatusRef());
//                ncCodeUpdateRequestSync.setNCCode(ncCodeIn);
//                com.sap.xi.me.NCCodeUpdateConfirmationMessageSync result = port.updateNCCode(ncCodeUpdateRequestSync);
//                System.out.println("Result = " + result);
//            } else {
//                System.out.println("nc code is null");
//            }
//        } catch (Exception ex) {
//            System.out.println(ex);
//            ex.printStackTrace();
//        }
//
//    }
//
//    private NCCode getNcCode(String ncCode) {
//        NCCode nCCode = new NCCode();
//        try { // Call Web Service Operation
//            com.sap.xi.me.NCCodeService service = new com.sap.xi.me.NCCodeService();
//            com.sap.xi.me.NCCodeProcessingIn port = service.getNCCodeServicePort();
//            Map<String, Object> context = ((BindingProvider) port).getRequestContext();
//            context.put(BindingProvider.USERNAME_PROPERTY, "wsuser");
//            context.put(BindingProvider.PASSWORD_PROPERTY, "idexx123");
//            System.out.println(String.format("WSDL Address: %s", context.get(BindingProvider.ENDPOINT_ADDRESS_PROPERTY)));
//            // TODO initialize WS operation arguments here
//            com.sap.xi.me.NCCodeByBasicDataQueryMessageSync ncCodeByBasicDataQuerySync = new com.sap.xi.me.NCCodeByBasicDataQueryMessageSync();
//            NCCodeRef nCCodeRef = new NCCodeRef();
//            SHORTName sHORTName = new SHORTName();
//            sHORTName.setValue(ncCode);
//            SiteRef ref = new SiteRef();
//            ref.setSite("USPB");
//            nCCodeRef.setSiteRef(ref);
//            nCCodeRef.setNcCode(sHORTName);
//            ncCodeByBasicDataQuerySync.setNCCodeByBasicDataQuery(nCCodeRef);
//            com.sap.xi.me.NCCodeByBasicDataResponseMessageSync result = port.findNCCodeByBasicData(ncCodeByBasicDataQuerySync);
//            nCCode = result.getNCCode();
//        } catch (Exception ex) {
//            System.out.println(ex);
//            ex.printStackTrace();
//        }
//        return nCCode;
//    }
//
//    public static void main(String[] args) {
//        new UpdateNCCode();
//    }
//
//}
