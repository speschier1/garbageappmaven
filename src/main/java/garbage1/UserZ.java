///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package garbage1;
//
//import au.com.bytecode.opencsv.CSVWriter;
//import com.idexx.utils.model.Employee;
//import com.idexx.utils.model.ADSAuthenticator;
//import java.io.FileWriter;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Set;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import javax.naming.NamingException;
//
///**
// *
// * @author speschier
// */
//public class UserZ {
//
//    private List<String> isolTemp = null;
//
//    public UserZ() throws IOException, NamingException {
//        String path = "C:\\USERS\\SPESCHIER\\DESKTOP\\USER_ACCESS.csv";
//        CSVWriter writer = new CSVWriter(new FileWriter(path), ',');
//        ADSAuthenticator ads = new ADSAuthenticator();
//        ads.setTrust("WESTBROOK");
//        List<String> missingGroups = new ArrayList<>();
//        String csvLine;
//        for (Employee emp : getEmployees()) {
//            missingGroups.clear();
//            csvLine = "";
//            Set<String> groups = ads.getUserGroups(ads.getEmployee(emp.getUsername()));
//            for (String isolTemplate : getISOLTemplate()) {
//                if (!groups.contains(isolTemplate)) {
//                    missingGroups.add(isolTemplate);
//                }
//            }
//            if (!missingGroups.isEmpty()) {
//                csvLine = emp.getUsername() + ",";
//                for (String group : missingGroups) {
//                    csvLine += group + ",";
//                }
//                csvLine = csvLine.substring(0, csvLine.length() - 1);
//                String[] entries = csvLine.split(",");
//                writer.writeNext(entries);
//            }
//        }
//        writer.close();
//    }
//
//    private List<Employee> getEmployees() throws NamingException {
//        List<Employee> isolEmployees = new ArrayList<>();
//
//        ADSAuthenticator ads = new ADSAuthenticator();
//        ads.setTrust("WESTBROOK");
//
//        Set<Employee> berniesDirects = ads.getManagersDirectReports("BGATCHELL");
//
//        for (Employee berniesDirect : berniesDirects) {
//            isolEmployees.addAll(ads.getManagersDirectReports(berniesDirect.getUsername()));
//        }
//
//        return isolEmployees;
//    }
//
//    private List<String> getISOLTemplate() {
//        if (isolTemp == null) {
//            isolTemp = new ArrayList<>();
//            isolTemp.add("G_eSOP_WX");
//            isolTemp.add("G_Orono_Lasercyte_Paperlesscal_WX");
//            isolTemp.add("G_Orono_Production_Active_WX");
//            isolTemp.add("G_Manufacturing_Prod_WX");
//            isolTemp.add("G_Manufacturing_Scripts_R");
//            isolTemp.add("G_PTPortalUsers");
//            isolTemp.add("GRP-VOL1-DEXTER");
//            isolTemp.add("GRP-VOL1-FOGHORN");
//            isolTemp.add("GRP-VOL2-DEXTER");
//            isolTemp.add("GRP-VOL2-FOGHORN");
//            isolTemp.add("GRP-VOL3-FOGHORN");
//            isolTemp.add("GRP-VOL4-FOGHORN");
//            isolTemp.add("GRP-VOL5-FOGHORN");
//            isolTemp.add("ISOL Floor Perms");
//            isolTemp.add("G_Service_WX");
//            isolTemp.add("WLANUsers");
//        }
//        return isolTemp;
//    }
//
//    public static void main(String[] args) {
//        try {
//            new UserZ();
//        } catch (IOException ex) {
//            Logger.getLogger(UserZ.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (NamingException ex) {
//            Logger.getLogger(UserZ.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
//
//}
